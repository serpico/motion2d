# Set SRC_subdir variable to all the files we want to parse during
# the build process. 
# Don't forget to update SRC_ALL variable if you add/remove a SRC_subdir 
# variable
#
# If you add/remove a directory, modify here
SET (SOURCE_COMPENSE
  src/compense/compense.cpp
)
SET (SOURCE_CONTRIB
  src/contrib/localpng/readpng.cpp 
  src/contrib/localpng/writepng.cpp
  src/contrib/mpeg2dec/motion.cpp
  src/contrib/mpeg2dec/display.cpp    
  src/contrib/mpeg2dec/mpeg2dec.cpp
  src/contrib/mpeg2dec/getbits.cpp    
  src/contrib/mpeg2dec/recon.cpp
  src/contrib/mpeg2dec/getblk.cpp     
  src/contrib/mpeg2dec/spatscal.cpp
  src/contrib/mpeg2dec/gethdr.cpp     
  src/contrib/mpeg2dec/store.cpp
  src/contrib/mpeg2dec/getpic.cpp     
  src/contrib/mpeg2dec/subspic.cpp
  src/contrib/mpeg2dec/getvlc.cpp     
  src/contrib/mpeg2dec/systems.cpp
  src/contrib/mpeg2dec/idct.cpp       
  src/contrib/mpeg2dec/verify.cpp
  src/contrib/mpeg2dec/idctref.cpp
  src/contrib/mpeg2dec/exttypes/dct.cpp
  src/contrib/mpeg2dec/exttypes/mvectors.cpp
  )
SET (SOURCE_CPRIM
  src/cprim/acast.cpp   
  src/cprim/fafirf3.cpp  
  src/cprim/ucaarith.cpp
  src/cprim/daarith.cpp 
  src/cprim/fafirf5.cpp  
  src/cprim/ucamem.cpp
  src/cprim/damem.cpp   
  src/cprim/fafirf7.cpp  
  src/cprim/uiafirf3.cpp
  src/cprim/faarith.cpp 
  src/cprim/famem.cpp 
  src/cprim/uiamem.cpp
  src/cprim/fafirf.cpp  
  src/cprim/saarith.cpp
  )
SET (SOURCE_ESTIMATION
  src/estimation/RMRmod.cpp 
  src/estimation/im_spat_temp.cpp
  src/estimation/cog.cpp     
  src/estimation/irls.cpp
  src/estimation/covariance.cpp
  src/estimation/mem_est.cpp
  src/estimation/estimate.cpp   
  src/estimation/para_mvt.cpp
  src/estimation/estimate_aff.cpp  
  src/estimation/variance.cpp
  src/estimation/estimate_const.cpp 
  src/estimation/weights.cpp
  src/estimation/estimate_quad.cpp
  )
SET (SOURCE_IMAGE
  src/image/CImageReader.cpp 
  src/image/CWriter.cpp
  src/image/CImageWriter.cpp        
  src/image/FieldVector.cpp
  src/image/CMotion2DVideo_Mpeg2.cpp
  src/image/Motion2DImage_PNG.cpp
  src/image/CMpeg2Reader.cpp  
  src/image/Motion2DImage_PNM.cpp
  src/image/CReader.cpp   
  src/image/Motion2DImage_RAW.cpp
  src/image/Motion2DImage_TIFF.cpp
  )
SET (SOURCE_INTERFACE
  src/interface/CMotion2D.cpp   
  src/interface/CMotion2DPyramid.cpp
  src/interface/CMotion2DEstimator.cpp 
  src/interface/CMotion2DWarping.cpp
  src/interface/CMotion2DModel.cpp
  )
SET (SOURCE_MAT_SYM
  src/mat_sym/arithm.cpp         
  src/mat_sym/mtx_tool.cpp
  src/mat_sym/inverse_mat_sym.cpp 
  src/mat_sym/resoud_mat_sym.cpp
  src/mat_sym/invert.cpp
  )
SET (SOURCE_MEMOIRE
  src/memoire/memoire.cpp
  )
SET (SOURCE_PYRAMIDE
  src/pyramide/filt_gauss.cpp  
  src/pyramide/multigr.cpp
  src/pyramide/gradient.cpp   
  src/pyramide/pyramide.cpp
  )
SET (SOURCE_MIDWAY
  src/midway/Midway.cpp  
  )
 SET (SOURCE_QUANTIFICATION
  src/quantification/quantification.cpp
  ) 
SET (SRC_ALL
  ${SOURCE_COMPENSE}
  ${SOURCE_CONTRIB}
  ${SOURCE_CPRIM}
  ${SOURCE_ESTIMATION}
  ${SOURCE_IMAGE}
  ${SOURCE_INTERFACE}
  ${SOURCE_MAT_SYM}
  ${SOURCE_MEMOIRE}
  ${SOURCE_PYRAMIDE}
  ${SOURCE_MIDWAY}
  ${SOURCE_QUANTIFICATION}
  )
