/*

  Copyright (c) 1995-2005 by INRIA.
  All Rights Reserved.

  This software was developed at:
  IRISA/INRIA Rennes
  Campus Universitaire de Beaulieu
  35042 Rennes Cedex

  http://www.irisa.fr

 */

/*
 * BackWarping.cpp
 * Author : Tristan Lecorgne
 * Mail : tristan.lecorgne@inria.fr
 * Description : Compense global movement using a motion2d result file
 */

#include <stdio.h>
#include <stdlib.h>
#ifdef __SunOS_
# include <iostream.h>
#else
# include <iostream>
#endif
#include <ctype.h>
#include <string>

// Includes lies a Motion2D
#include <CMotion2DImage.h>
#include <CMotion2DModel.h>
#include <CMotion2DWarping.h>
#include <CReader.h>
#include <CImageReader.h>
#include <CMpeg2Reader.h>
#include "CImageWriter.h"
#include "../src/inc/type.h"
#include "../src/compense/compense.h"

using namespace std;

#define GETOPTARGS	"f:hi:m:p:s:vw:?"
#define max(a,b) (a>b?a:b)

bool findChar(string ch, const char *f)
{
	int n = ch.find(f);
	int size = ch.size();
	return (n>0 && n<size);
};

int  getoption (int argc, char** argv, char* pszValidOpts, char** ppszParam);
void getoptions(int argc, char **argv,
		string &ipath, unsigned long &frame,
		int &step, long unsigned &niter,
		string &mfile, string &wpath, bool &verbose);
void usage(char *name, char *badparam);
bool readModel(string filename, unsigned long frame, CMotion2DModel &model);
string readModelId (string filename);
#define DEBUG_LEVEL1 0
#define DEBUG_LEVEL2 0


/*

  Compense sequence using motion model

 */
int main(int argc, char **argv)
{
	CMotion2DImage<unsigned char> I;	// Input image
	CMotion2DImage<unsigned char> B;	// Backwarped image
	CMotion2DModel	model, modelBis;		// Parametric motion model
	CMotion2DWarping	warping;	// Warping
	CReader		*Ireader=NULL;	// Image reader
	CImageReader		imgr;		// PNM or PNG or TIFF image reader
	CMpeg2Reader		mpegr;		// Mpeg2 image decoder
	CWriter		*Iwriter=NULL;	// Image writer
	CImageWriter		imgw;		// PNM or PNG or TIFF image writer

	string ipath = "../../test/sequence/rond-point%04d.png"; // Image path
	string filename;		// Complete filename for an image of the video
	string mfile;			// Motion model filename
	string bpath;			// Backwarped image path
	long unsigned niter = 33;	// Number of images to process
	int  step = 1;		// Step between 2 images
	bool verbose = false;		// Verbose mode
	bool ierr = true;		// Image IO return value
	unsigned long frame  = 1; // Current frame number to process

	// Read the program options
	getoptions(argc, argv, ipath, frame, step, niter,
			mfile, bpath, verbose);

	// Set the image reader format by regarding the image extension
	if (findChar(ipath, _mpg) || findChar(ipath, _mpeg)) {
		if (verbose) cout << "Reader mpeg\n";
		Ireader = &mpegr;
	} else {
		if (verbose) cout << "Reader image\n";
		Ireader = &imgr;
	}

	// Test reader
	Ireader->setFileName(ipath);
	Ireader->setFrameNumber(frame);
	filename = Ireader->getFileName();
	if (!Ireader->openStream()) {
		cout <<"Can't open stream "<< ipath <<endl;
		exit(-1);
	}

	// Set the image writer
	Iwriter = &imgw;

	// Load first image
	Ireader->setFileName(ipath);
	Ireader->setFrameNumber(frame);
	filename = Ireader->getFileName();
	if (verbose)
		cout << "Load image: " << filename << endl ;
	ierr = Ireader->getFrame(I);
	if (ierr == false) {
		cout << "Can not read the image: " << filename << endl;
		exit(-1);
	}

	// Read the model type
	string modeltype = readModelId(mfile);

	// Create model
	model.reset();	// Set all the parameters to zero
	model.setIdModel(modeltype); // Set the model id TODO change
	model.setConstVarLight(false); // Set the illumination variation estimation
	model.setExpVarLight(false); // Set the illumination variation estimation
	model.setOrigin(-1, -1); // Set the origin

	// Backwarp the first image, taken as image reference
	B.Init(I.GetRows(), I.GetCols());
	warping.initBackWarp(B.GetRows(), B.GetCols(), 0, 0);
	warping.backWarp(I.bitmap, I.GetRows(), I.GetCols(), B.bitmap, model);

	// Write the backwarped image
	Iwriter->setFileName(bpath);
	Iwriter->setFrameNumber(frame);
	filename = Iwriter->getFileName();
	if (verbose)
		cout << "\tWrite backwarped image: " << filename << endl ;
	ierr = Iwriter->writeFrame(B);
	if (ierr == false) {
		cout << "Can not write the image: " << filename << endl;
		exit(-1);
	}

	// Start the backwarping loop
	long unsigned i=0;
	do {

		// Read the line of the model corresponding to the frame
		if (readModel(mfile, frame, model) == false)
			exit(0);

		// Update the image frame number
		frame += step;

		// Load the image indexed by "frame"
		Ireader->setFileName(ipath);
		Ireader->setFrameNumber(frame);
		filename = Ireader->getFileName();
		if (verbose)
			cout << "Load image: " << filename << endl ;
		ierr = Ireader->getFrame(I);
		if (ierr == false) {
			cout << "Can not read the image: " << filename << endl;
			exit(-1);
		}

		// Backwarp image
		warping.backWarp(I.bitmap, I.GetRows(), I.GetCols(), B.bitmap, model);

		// Save the backwarp image
		Iwriter->setFileName(bpath);
		Iwriter->setFrameNumber(frame);
		filename = Iwriter->getFileName();
		if (verbose)
			cout << "\tWrite backwarped image: " << filename << endl ;
		ierr = Iwriter->writeFrame(B);    // Write the backwarped image
		if (ierr == false) {
			cout << "Can not write the backwarped image: " << filename << endl;
			exit(-1);
		}


	} while (++i < niter);

	Ireader->closeStream();

	return 0;
}



/*

  Print the program options.

 */
void usage(char *name, char *badparam)
{
	if (badparam)
		fprintf(stderr, "\nBad parameter [%s]\n", badparam);

	cout << "\nCopyright (c) 1995-2005 by INRIA.\n";
	cout << "All Rights Reserved.\n";
	cout << "\n";
	cout << "This software was developed at:\n";
	cout << "IRISA/INRIA Rennes\n";
	cout << "Campus Universitaire de Beaulieu \n";
	cout << "35042 Rennes Cedex\n";
	cout << "\n";
	cout << "http://www.irisa.fr\n";
	cout << "\n";
	cout << "SYNOPSIS\n\t";
	cout << name;
	cout << "[-p image_path] [-f first_frame]\n";
	cout << "\t[-s step] [-i iterations] [-m model_filename] [-w warped_image_path]\n";
	cout << "\t[-v] [-h] [-?]\n";
	cout << "\n";
	cout << "DESCRIPTION\n";
	cout << "\tThis software generates warped images using a parametric motion model.\n";
	cout << "\n";
	cout << "\n";

	string pngTiff = "";
	string tiffInput = "";
	string tiffOutput = "";
#ifndef __NO_IMAGEIO_PNG_
	pngTiff += "PNG, ";
#endif
#ifndef __NO_IMAGEIO_TIFF
	pngTiff += "TIFF ";
	tiffInput += "\tNote: TIFF input sequences HAVE TO be multipage and grayscale\n";
	tiffOutput += "\tNote: Motion2D don't generate multipage TIFF. So if\n\tyou want a TIFF output, you need to specify a generic name\n\tas well\n";
#endif

	cout << "INPUT SEQUENCE OPTIONS: 				Default\n";
	cout << "\n";
	cout << "-p image_path [%%s]                  ../../test/sequence/rond-point\n";
	cout << "\tSpecify the path and the generic name of the files \n";
	cout << "\tcontaining the images to process. The following image\n";
	cout << "\tfile formats " << pngTiff << "and PNM are supported. The different\n";
	cout << "\tPNM formats are PGM (P5) and PPM (P6).\n";
	cout << tiffInput << "\n";

	cout << "-f first_frame [%%s]                                           0001\n";
	cout << "\tSpecify the number of the first frame in the video \n";
	cout << "\tsequence. If the image sequence numbering uses a fixed \n";
	cout << "\tnumber of digits, complete whith 0 before the image number.\n";
	cout << "\n";
	cout << "-s step [%%d]                                                     1\n";
	cout << "\tSpecify the step between two frames in the video sequence.\n";
	cout << "\tIf step > 0 images are processed forward. If step < 0 images\n";
	cout << "\tare processed backward.\n";
	cout << "\n";
	cout << "-i iterations [%%lu]                                             33\n";
	cout << "\tSpecify the number of motion estimation iterations to\n";
	cout << "\tprocess. The number of the last image computed is given by:\n";
	cout << "\tfirst_frame + iterations * step.\n";
	cout << "\n";
	cout << "-m model_filename [%%s]                       \n";
	cout << "\tSpecify the name of the file containing the values of the\n";
	cout << "\tparametric motion model coefficients.\n";
	cout << "\n";
	cout << "\n";
	cout << "RESULTS OPTIONS:\n";
	cout << "\n";
	cout << "-w warped_image_path [%%s]                           \n";
	cout << "\tSpecify the path and the generic name of the files contain-\n";
	cout << "\ting the back-warped images built using the estimated motion\n";
	cout << "\tmodel." << endl;
	cout << tiffOutput;

	cout << "OTHER OPTIONS:\n";
	cout << "\n";
	cout << "-v\n";
	cout << "\tActivate the verbose mode.\n";
	cout << "\n";
	cout << "-h\n";
	cout << "\tPrint the help.\n";
	cout << "\n";
	cout << "-?\n";
	cout << "\tPrint the help.\n";
	cout << "\n";

	exit(0);
}

/*

  Set the program options.

 */
void getoptions(int argc, char **argv,
		string &ipath, unsigned long &frame,
		int &step, long unsigned &niter,
		string &mfile, string &wpath, bool &verbose)
{
	char *optarg;
	int	c;
    while ((c = getoption(argc, argv, (char *)GETOPTARGS, &optarg)) > 1) {

		switch (c) {
		case 'f': frame = (unsigned long) atoi(optarg); break;
		case 'h': usage(argv[0], NULL); break;
		case 'i': niter = atoi(optarg); break;
		case 'm': mfile = optarg; break;
		case 'p': ipath = optarg; break;
		case 's': step = atoi(optarg); break;
		case 'v': verbose = true; break;
		case 'w': wpath = optarg; break;
		case '?': usage(argv[0], NULL); break;

		default:  usage(argv[0], NULL); break;
		}
	}

	// Some option tests
	if (wpath.empty()) {
		// standalone param or error
		fprintf(stderr, "Option -w [warped_image_path] not specified\n");
		usage(argv[0], NULL);
		exit(0);


	}
	if ((c == 1) || (c == -1)) {
		// standalone param or error
		fprintf(stderr, "Bad argument %s\n", optarg);

		usage(argv[0], NULL);

		exit(0);
	}

	if ( mfile.empty() ) {
		cout << endl << "Error: argument -m <filename> not specified " << endl;
		exit(0);
	}
	else {
		FILE *fd = fopen(mfile.c_str(), "rb");
		if (fd == NULL) {
			cout << endl
					<< "Bad argument -m: "
					<< "Cannot open the filename specified "
					<< endl
					<< "with this option."
					<< endl;
			exit(0);
		}
		fclose(fd);
	}
}

/*

  Get next command line option and parameter

  PARAMETERS:

      argc - count of command line arguments
      argv - array of command line argument strings
      pszValidOpts - string of valid, case-sensitive option characters,
                     a colon ':' following a given character means that
                     option can take a parameter
      ppszParam - pointer to a pointer to a string for output

  RETURNS:

      If valid option is found, the character value of that option
          is returned, and *ppszParam points to the parameter if given,
          or is NULL if no param
      If standalone parameter (with no option) is found, 1 is returned,
          and *ppszParam points to the standalone parameter
      If option is found, but it is not in the list of valid options,
          -1 is returned, and *ppszParam points to the invalid argument
      When end of argument list is reached, 0 is returned, and
 *ppszParam is NULL
 */
int getoption (
		int argc,
		char** argv,
        char* pszValidOpts,
		char** ppszParam)
{
	static int iArg = 1;
	int chOpt;
	char* psz = NULL;
	char* pszParam = NULL;

	if (iArg < argc) {
		psz = &(argv[iArg][0]);
		if (*psz == '-') { // || *psz == '/')  {
			// we have an option specifier
			chOpt = argv[iArg][1];
			if (isalnum(chOpt) || ispunct(chOpt)) {
				// we have an option character
				psz = strchr(pszValidOpts, chOpt);
				if (psz != NULL) {
					// option is valid, we want to return chOpt
					if (psz[1] == ':') {
						// option can have a parameter
						psz = &(argv[iArg][2]);
						if (*psz == '\0') {
							// must look at next argv for param
							if (iArg+1 < argc) {
								psz = &(argv[iArg+1][0]);
								// next argv is the param
								iArg++;
								pszParam = psz;
							}
							else {
								// reached end of args looking for param
							}

						}
						else {
							// param is attached to option
							pszParam = psz;
						}
					}
					else {
						// option is alone, has no parameter
					}
				}
				else {
					// option specified is not in list of valid options
					chOpt = -1;
					pszParam = &(argv[iArg][0]);
				}
			}
			else {
				// though option specifier was given, option character
				// is not alpha or was was not specified
				chOpt = -1;
				pszParam = &(argv[iArg][0]);
			}
		}
		else {
			// standalone arg given with no option specifier
			chOpt = 1;
			pszParam = &(argv[iArg][0]);
		}
	}
	else {
		// end of argument list
		chOpt = 0;
	}

	iArg++;
	*ppszParam = pszParam;
	return (chOpt);
}

bool readModel(string filename, unsigned long frame, CMotion2DModel &model)
{
#define MAX_LEN 512

	FILE *fd = NULL;
	char  str[MAX_LEN];
	int   line;
	char* cerr;
	int   ierr;

	if ( filename.empty() ) {
		fprintf(stderr, "Error in readModel: no filename\n");
		return false;
	}

	fd = fopen(filename.c_str(), "rb");
	if (fd == NULL) {
		fprintf(stderr, "Error in readModel: couldn't read file %s\n",
				filename.c_str());
		return false;
	}

	// Jump the possible comment, or empty line and read the following line
	line = 0;
	do {
		cerr = fgets(str, MAX_LEN - 1, fd);

		line++;
		if (cerr == NULL) {
			fprintf(stderr, "Error in readModel: couldn't read line %d of file %s\n",
					line, filename.c_str());
			fclose (fd);
			return false;
		}
	} while ((str[0] == '#') || (str[0] == '\n'));

	// Extract model id
	char _text[255];
	char _egal[255];
	char _id[255];
	string s = str;
	ierr = s.find("MODEL_ID");
	if (ierr != -1) {
		ierr = sscanf(str, "%s %s %s", _text, _egal, _id);
		//    printf("text: %s\n", _text);
		//   printf("_egal: %s\n", _egal);
		//  printf("_id: %s\n", _id);
	}
	else {
		ierr = sscanf(str, "%s", _id);
	}

	if (ierr == EOF) {
		fprintf(stderr,
				"Error in readModel: premature EOF on line %d of file %s\n",
				line, filename.c_str());
		fclose (fd);
		return false;
	}

	unsigned long _frame;
	double _row, _col; // model origin
	double _p[CMotion2DModel::MDL_NMAX_COEF]; // model parameters
	double _multfactor = 1.0;
	bool multfactor_was_read = false;

	do {
		// Read a line
		cerr = fgets(str, MAX_LEN - 1, fd);
		//printf("line2: %s\n", str);
		line ++;
		if (cerr == NULL) {
			fprintf(stderr, "Error in readModel: couldn't read line %d of file %s\n",
					line, filename.c_str());
			fclose (fd);
			return false;
		}

		// Get the multiplier factor if it exists
		if (multfactor_was_read == false) {
			s = str;
			ierr = s.find("MULTIPLIER_FACTOR");
			multfactor_was_read = true;
			if (ierr != -1) {
				ierr = sscanf(str, "%s %s %lf", _text, _egal, &_multfactor);
				if (ierr == EOF) {
					fprintf(stderr,
							"Error in readModel: premature EOF on line %d of file %s\n",
							line, filename.c_str());
					fclose (fd);
					return false;
				}
				continue;
			}
		}

		// Extract information
		ierr = sscanf(str,
				"%ld %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf",
				&_frame, &_row, &_col,
				&_p[0], &_p[1], // c1, c2
				&_p[2], &_p[3], &_p[4], &_p[5], //a1, a2, a3, a4
				&_p[6], &_p[7], &_p[8], &_p[9], &_p[10], &_p[11]); //q1...q6
		if (ierr == EOF) {
			fprintf(stderr,
					"Error in readModel: premature EOF on line %d of file %s\n",
					line, filename.c_str());
			fclose (fd);
			return false;

		}
	} while (frame != _frame);

	// rescale the parameters
	for (int i=0; i < CMotion2DModel::MDL_NMAX_COEF; i ++) {
		_p[i] /= _multfactor;
	}

	string id = _id;

	model.reset();	// Set all the parameters to zero
	model.setIdModel(id); // Set the model id
	model.setConstVarLight(false); // Set the illumination variation
	model.setExpVarLight(false); // Set the illumination variation
	model.setOrigin(_row, _col); // Set the origin
	model.setParameters(_p);

	fclose (fd);

	return true;

#undef MAX_LEN
}

string readModelId (string filename) {
#define MAX_LEN 512

	FILE *fd = NULL;
	char  str[MAX_LEN];
	int   line;
	char* cerr;
	int   ierr;

	if ( filename.empty() ) {
		fprintf(stderr, "Error in readModel: no filename\n");
		return "";
	}

	fd = fopen(filename.c_str(), "rb");
	if (fd == NULL) {
		fprintf(stderr, "Error in readModel: couldn't read file %s\n",
				filename.c_str());
		return "";
	}

	// Jump the possible comment, or empty line and read the following line
	line = 0;
	do {
		cerr = fgets(str, MAX_LEN - 1, fd);

		line++;
		if (cerr == NULL) {
			fprintf(stderr, "Error in readModel: couldn't read line %d of file %s\n",
					line, filename.c_str());
			fclose (fd);
			return "";
		}
	} while ((str[0] == '#') || (str[0] == '\n'));

	char _text[255];
	char _egal[255];
	char _id[255];

	string s = str;
	ierr = s.find("MODEL_ID");
	if (ierr != -1) {
		ierr = sscanf(str, "%s %s %s", _text, _egal, _id);
	}
	else {
		ierr = sscanf(str, "%s", _id);
	}

	string id = _id;
	return id;
}
