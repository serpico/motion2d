/*

  Copyright (c) 1995-2005 by INRIA.
  All Rights Reserved.

  This software was developed at:
  IRISA/INRIA Rennes
  Campus Universitaire de Beaulieu
  35042 Rennes Cedex

  http://www.irisa.fr

*/

#include <stdio.h>
#include <stdlib.h>
#ifdef __SunOS_
# include <iostream.h>
#else
# include <iostream>
#endif
#include <ctype.h>
#include <string>

// Include for Motion2D
#include <CMotion2DImage.h>
#include <CMotion2DEstimator.h>
#include <CMotion2DPyramid.h>
#include <CMotion2DModel.h>
#include <CMotion2DWarping.h>

using namespace std;

#define GETOPTARGS	"a:b:c:e:f:ghi:j:k:l:m:n:p:r:s:t:u:vw:x:y:z?"
#define max(a,b) (a>b?a:b)

string setfilename(string ipath, long frame,
		   unsigned ndigits, string ext);
void printresults(FILE *output, unsigned long i1, unsigned long i2,
		  CMotion2DModel model, double support_size);
int  getoption (int argc, char** argv, char* pszValidOpts, char** ppszParam);
void getoptions(int argc, char **argv,
		string &ipath, string &ext,
		string &first, int &step, long unsigned &niter,
		string &model_id, bool &const_var_light,
		bool &model_orig_fixed,
		double &model_row_orig, double &model_col_orig,
		unsigned &pyr_nlevels, unsigned &pyr_stop_level,
		bool &compute_covariance,
		string &rpath, string &bpath,
		int &b_nrows, int &b_ncols, int &b_row_orig, int &b_col_orig,
		string &wpath, string &spath, int &slabel, bool &verbose);
void usage(char *name, char *badparam);


enum EImageFormat {
  PGM,
  PPM
#ifndef __NO_IMAGEIO_PNG_
  ,PNG
#endif
};

/*

  Robust multiresolution estimation of parametric motion model.

*/
int main(/*int argc, char **argv*/)
{
  CMotion2DImage<unsigned char> I;	// Image
  CMotion2DImage<unsigned char> S;	// Motion estimator support
  CMotion2DEstimator	estimator;	// Motion estimator
  CMotion2DModel	model;		// Parametric motion model
  CMotion2DPyramid	pyramid1;	// Pyramid on image 1
  CMotion2DPyramid	pyramid2;	// Pyramid on image 2

  string ipath; // Image path
  string spath; // Estimation support path
  string filename;		// Complete filename for an image of the video
  string first = "1";	// First frame number to process
  string ext = ".pgm";	// Image extension
  long unsigned niter = 1;	// Number of images to process
  int  step = 1;		// Step between 2 images
  unsigned char label = 87;	// Value of the motion estimator support
  string model_id = "AC";	// Parametric motion model ID to estimate
  bool const_var_light = false;	// Lighting variation parameter estimation
  bool exp_var_light = false;	// Lighting variation parameter estimation
  unsigned pyr_nlevels = 1;	// Number of levels in a multi-resolution pyr
  unsigned pyr_stop_level = 0;	// Pyramid level where the estimator stops
  bool ierr = true;		// Image IO return value
  TPyramidError state_pyr;
  double support_size;


  // Initialize the parameters
  unsigned long frame  = atoi(first.c_str()); // Current frame number processed
  unsigned ndigits_in  = first.length();// Number of digits coding image number
  char last[256];
  sprintf(last, "%ld", frame + step * niter);

  // Initialisation of the 2D parametric motion model to estimate
  model.reset();	// Set all the parameters to zero
  model.setIdModel(model_id); // Set the model id
  model.setConstVarLight(const_var_light); // Set the illumination variation estimation
  model.setExpVarLight(exp_var_light); // Set the illumination variation estimation

  // Origin fixed at the middle of the image
  model.setOrigin(I.GetRows() / 2.0, I.GetCols() / 2.0); // Set the origin

  for (int i=31; i <= 38; i ++) {
//      if (i < 34)   pyr_nlevels = 3;
//      else if (i < 36) pyr_nlevels = 2;

    char fileimagette[FILENAME_MAX];
    char filesupport[FILENAME_MAX];
    sprintf(fileimagette, "/udd/fspindle/poub/debug/imagette00%d-", i);
    sprintf(filesupport, "/udd/fspindle/poub/debug/support00%d-", i);

    ///////////////////////////////// test 1
    //ipath = "/udd/fspindle/poub/debug/imagette0036-"; // Image path
    //spath = "/udd/fspindle/poub/debug/support0036-"; // Estimation support path
    ipath = fileimagette;
    spath = filesupport;

    // Initialize the motion estimator
    // Set the pyramid start estimation level
    if (i < 34) estimator.setFirstEstimationLevel(3);
    else if (i < 36) estimator.setFirstEstimationLevel(2);
    else estimator.setFirstEstimationLevel(1);

    //estimator.setFirstEstimationLevel(pyr_nlevels - 1);

    cout << "Specified estimator start level: "
	 << estimator.getFirstEstimationLevel() << endl;

    //estimator.setFirstEstimationLevel(5);
    // Set the pyramid stop estimation level
    estimator.setLastEstimationLevel(pyr_stop_level);
    estimator.setRobustEstimator(true);

    // Load the support
    filename = setfilename(spath, frame, ndigits_in, ext);
    cout << "Load support: " << filename.c_str() << endl;
    ierr = ReadPGM(S, filename.c_str()); // Load the image PGM P5 file
    if (ierr == false) exit(-1);

  // Pyramids 1 constrcution
    filename = setfilename(ipath, frame, ndigits_in, ext);
    cout << "Load image 1: " << filename.c_str() << endl;
    ierr = ReadPGM(I, filename.c_str()); // Load the image PGM P5 file
    if (ierr == false) exit(-1);
    //state_pyr = pyramid1.build(I.bitmap, I.GetRows(), I.GetCols(), estimator.getFirstEstimationLevel()+1);
    state_pyr = pyramid1.build(I.bitmap, I.GetRows(), I.GetCols(), pyr_nlevels);
    if (state_pyr != PYR_NO_ERROR)
      exit(-1);

  // Pyramids 2 constrcution
    filename = setfilename(ipath, frame + 1, ndigits_in, ext);
    cout << "Load image 2: " << filename.c_str() << endl;
    ierr = ReadPGM(I, filename.c_str()); // Load the image PGM P5 file
    if (ierr == false) exit(-1);
    //state_pyr = pyramid2.build(I.bitmap, I.GetRows(), I.GetCols(), estimator.getFirstEstimationLevel()+1);
    state_pyr = pyramid2.build(I.bitmap, I.GetRows(), I.GetCols(), pyr_nlevels);
    if (state_pyr != PYR_NO_ERROR)
      exit(-1);

    cout << "Motion estimation in process....." << endl;
    bool state_est;
    state_est = estimator.estimate(pyramid1, pyramid2, S.bitmap, label, model);
    if (state_pyr == false){
      pyramid1.destroy();
      pyramid2.destroy();
      exit(0);
    }

    cout << "Used estimator start level: "
	 << estimator.getFirstEstimationLevel() << endl;

    // Print results
    if (! estimator.getSupportSize(support_size)) support_size = -1.;
    printresults(stdout, frame - step, frame, model, support_size);
    if ( estimator.isResidualVarianceComputed() )
      cout << "\tResidual variance: "
	   << estimator.getResidualVariance() << endl;

    pyramid1.destroy();
    pyramid2.destroy();

  }

  return 0;
}

/*
  Build the filename of an image.
*/
string setfilename(string ipath, long frame,
		   unsigned ndigits, string ext)
{
  char buf[255];
  sprintf(buf, "%ld", frame);
  string zeros;
  zeros += "0000000000";
  zeros += buf;

  unsigned lng = max(ndigits, strlen(buf));

  string filename;
  filename += ipath;
  filename += zeros.substr(zeros.size()-lng, lng);
  filename += ext;

  return filename;
}

/*
  Print results.

*/
void printresults(FILE *output, unsigned long i1, unsigned long i2,
		  CMotion2DModel model, double support_size)
{

  double row, col;
  double parameters[CMotion2DModel::MDL_NMAX_COEF];
  double const_var_light, exp_var_light;
  model.getOrigin(row, col);
  model.getParameters(parameters);

  if (output == stdout) {
    // Print the motion estimation results
    fprintf(output, "\t2D estimated model between images %lu and %lu:\n\t",
	    i1, i2);

    fprintf(output, "origin: %f %f\n\t", row, col);

    string s = model.idToString();
    fprintf(output, "motion model: %s\n", s.c_str());
    fprintf(output, "\tx: %f | %f %f | %f %f %f \n",
	    parameters[0], parameters[2], parameters[3],
	    parameters[6], parameters[7], parameters[8]);
    fprintf(output, "\ty: %f | %f %f | %f %f %f \n",
	    parameters[1], parameters[4], parameters[5],
	    parameters[9], parameters[10], parameters[11]);

    if (model.getConstVarLight(const_var_light))
      fprintf(output, "\tconstant global illumination: %f \n", const_var_light);
    if (model.getExpVarLight(exp_var_light))
      fprintf(output, "\texp global illumination: %f \n", exp_var_light);

    if (support_size != -1.)
      fprintf(output, "\tsupport size: %f \n", support_size);
  }
  else {
    fprintf(output, "%lu %f %f ", i1, row, col);
    for (int i = 0; i < CMotion2DModel::MDL_NMAX_COEF; i++)
      fprintf(output, "%f ", parameters[i]);
    if (!model.getConstVarLight(const_var_light)) const_var_light = 0;
      fprintf(output, "%f ", const_var_light);
    if (support_size != -1.)
      fprintf(output, "%f ", support_size);
    if (!model.getExpVarLight(exp_var_light)) exp_var_light = 0;
      fprintf(output, "%f ", exp_var_light);
    fprintf(output, "\n");
  }
  fflush(output);
}


