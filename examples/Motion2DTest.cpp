/*

  Copyright (c) 1995-2005 by INRIA.
  All Rights Reserved.

  This software was developed at:
  IRISA/INRIA Rennes
  Campus Universitaire de Beaulieu
  35042 Rennes Cedex

  http://www.irisa.fr

*/

#include <stdio.h>
#include <stdlib.h>
#ifdef __SunOS_
# include <iostream.h>
#else
# include <iostream>
#endif
#include <ctype.h>
#include <string>

// Include for Motion2D
#include <CMotion2DImage.h>
#include <CMotion2DEstimator.h>
#include <CMotion2DPyramid.h>
#include <CMotion2DModel.h>
#include <CMotion2DWarping.h>

using namespace std;

#define GETOPTARGS	"a:b:c:e:f:gGhi:j:k:l:m:n:p:r:s:t:u:vw:x:y:z?"
#define max(a,b) (a>b?a:b)

string setfilename(string ipath, long frame,
		   unsigned ndigits, string ext);
void printresults(FILE *output, unsigned long i1, unsigned long i2,
		  CMotion2DModel model, double support_size);
int  getoption (int argc, char** argv, char* pszValidOpts, char** ppszParam);
void getoptions(int argc, char **argv,
		string &ipath, string &ext,
		string &first, int &step, long unsigned &niter,
		string &model_id, bool &const_var_light, bool &exp_var_light,
		bool &model_orig_fixed,
		double &model_row_orig, double &model_col_orig,
		unsigned &pyr_nlevels, unsigned &pyr_stop_level,
		bool &compute_covariance,
		string &rpath, string &bpath,
		int &b_nrows, int &b_ncols, int &b_row_orig, int &b_col_orig,
		string &wpath, string &spath, int &slabel, bool &verbose);
void usage(char *name, char *badparam);


enum EImageFormat {
  PGM,
  PPM
#ifndef __NO_IMAGEIO_PNG_
  ,PNG
#endif
};

/*

  Robust multiresolution estimation of parametric motion model.

*/
int main(int argc, char **argv)
{
  CMotion2DImage<unsigned char> I;	// Image
  CMotion2DImage<unsigned char> S;	// Motion estimator support
  CMotion2DImage<unsigned char> B;	// Backwarped image
  CMotion2DImage<unsigned char> W;	// M-estimator weights image
  CMotion2DEstimator	estimator;	// Motion estimator
  CMotion2DModel	model;		// Parametric motion model
  CMotion2DPyramid	pyramid1;	// Pyramid on image 1
  CMotion2DPyramid	pyramid2;	// Pyramid on image 2
  CMotion2DWarping	warping;	// Warping

  string ipath = "../../test/sequence/rond-point"; // Image path
  string ext = ".pgm";		// Image extension
  string filename;		// Complete filename for an image of the video
  string first = "0001";	// First frame number to process
  string rpath;			// Result filename to store the model
  string bpath;			// Back-warped image path
  string wpath;			// M-estimator weights image path
  string spath;			// Estimation support path
  FILE   *rfile = NULL;		// Result file to store the model
  long unsigned niter = 33;	// Number of images to process
  int  step = 1;		// Step between 2 images
  unsigned char label = 234;	// Value of the motion estimator support
  int slabel = -1;		// Value of the motion estimator support
  string model_id = "AC";	// Parametric motion model ID to estimate
  bool const_var_light = false;	// Lighting variation parameter estimation
  bool exp_var_light = false;	// Lighting variation parameter estimation
  bool model_orig_fixed = false;// Indicates if an origin is fixed
  double model_row_orig = -1.;  // Motion model origin (line coordinate)
  double model_col_orig = -1.;  // Motion model origin (column coordinate)
  unsigned pyr_nlevels = 4;	// Number of levels in a multi-resolution pyr
  unsigned pyr_stop_level = 0;	// Pyramid level where the estimator stops
  bool verbose = false;		// Verbose mode
  bool support_empty = false;   // Indicates if the support is empty.
  EImageFormat format = PGM;    // Image format (PGM, PPM,...)
  bool ierr = true;		// Image IO return value
  int b_ncols=-1, b_nrows=-1;	// Back-warped image size
  int b_col_orig=0, b_row_orig=0;	// Back-warped origin in image frame
  bool compute_covariance = false; // Flag to compute the covariance matrix

  double dep_row[4], dep_col[4];
  double pos_row[4], pos_col[4];

  // Read the program options
  getoptions(argc, argv,
	     ipath, ext, first, step, niter,
	     model_id, const_var_light, exp_var_light,
	     model_orig_fixed, model_row_orig, model_col_orig,
	     pyr_nlevels, pyr_stop_level, compute_covariance,
	     rpath,
	     bpath, b_nrows, b_ncols, b_row_orig, b_col_orig,
	     wpath, spath, slabel, verbose);

  // Initialize the parameters
  unsigned long frame  = atoi(first.c_str()); // Current frame number processed
  unsigned ndigits_in  = first.length();// Number of digits coding image number
  unsigned ndigits_out; // Number of digits coding image number
  char last[256];
  sprintf(last, "%ld", frame + step * niter);
  ndigits_out = strlen(last);

  // Set the image format by regarding the image extension
  if (ext == ".pgm")      format = PGM;
  else if (ext == ".ppm") format = PPM;
#ifndef __NO_IMAGEIO_PNG_
  else if (ext == ".png") format = PNG;
  else {
    cerr << "Error: Only PNG and PNM (PGM P5 and PPM P6) image format "
	 << endl << "are implemented." << endl;
    exit(-1);
  }
#else
  else {
    cerr << "Error: Only PNM (PGM P5 and PPM P6) image format "
	 << endl << "are implemented." << endl;
    exit(-1);
  }
#endif


  filename = setfilename(ipath, frame, ndigits_in, ext);

  if (verbose)
    cout << "Load image: " << filename.c_str() << endl;
  else
    cout << "Motion estimation in process....." << endl;

  switch (format) {
  case PGM:
    ierr = ReadPGM(I, filename.c_str()); // Load the image PGM P5 file
    break;
  case PPM:
    ierr = ReadPPM(I, filename.c_str()); // Load the image PPM P6 file
    break;
#ifndef __NO_IMAGEIO_PNG_
  case PNG:
    ierr = ReadPNG(I, filename.c_str()); // Load the image PNG file
    break;
#endif
  }
  if (ierr == false) exit(-1);

  // Initialisation of the 2D parametric motion model to estimate
  model.reset();	// Set all the parameters to zero
  model.setIdModel(model_id); // Set the model id
  model.setConstVarLight(const_var_light); // Set the illumination variation estimation
  model.setExpVarLight(exp_var_light); // Set the illumination variation estimation

  // Initialize the model origin
  if ( ! model_orig_fixed) {
    // Origin fixed at the middle of the image
    model_row_orig = I.GetRows() / 2.0;
    model_col_orig = I.GetCols() / 2.0;
  }

  model.setOrigin(model_row_orig, model_col_orig); // Set the origin

  // Pyramids allocation for two successive images
  pyramid1.allocate(I.GetRows(), I.GetCols(), pyr_nlevels);
  pyramid2.allocate(I.GetRows(), I.GetCols(), pyr_nlevels);

  // Fill the pyramid 1 whith image 1
  pyramid1.build(I.bitmap);

  // Initialize the motion estimator
  // Set the pyramid start estimation level
  estimator.setFirstEstimationLevel(pyr_nlevels - 1);
  // Set the pyramid stop estimation level
  estimator.setLastEstimationLevel(pyr_stop_level);
  estimator.setRobustEstimator(true);

  if (compute_covariance)
    estimator.computeCovarianceMatrix(true);

  // Initialize the support
  if (spath.empty()) {
    // Set the estimator support to the whole image
    S.Init(I.GetRows(), I.GetCols(), label);
  }
  else {
    // Set the support label value
    label = (unsigned char) slabel;
  }

  if ( !bpath.empty() ) {
    // Set the backwarped image size
    //B.Init(I.GetRows() +40, I.GetCols() +200 );
    // Init warping by positionning the reference image in the backwarped img.
    //warping.initBackWarp(B.GetRows(), B.GetCols(), -20, -20);

    // Set the backwarped image size
    if (b_nrows <= 0) b_nrows = I.GetRows();
    if (b_ncols <= 0) b_ncols = I.GetCols();
    B.Init(b_nrows, b_ncols);
    warping.initBackWarp(B.GetRows(), B.GetCols(), b_row_orig, b_col_orig);

    // Backwarp the first image, taken as image reference
    warping.backWarp(I.bitmap, I.GetRows(), I.GetCols(), B.bitmap, model);
    {
      CMotion2DModel model_inv;
      if (model.invert(model_inv) == true)

      {
        double row, col;
	double parameters[CMotion2DModel::MDL_NMAX_COEF];
	model_inv.getOrigin(row, col);
	model_inv.getParameters(parameters);

	// Print the motion estimation results
	fprintf(stdout, "\tinverted model \n\t");

	fprintf(stdout, "origin: %f %f\n\t", row, col);

	string s = model_inv.idToString();
	fprintf(stdout, "motion model: %s\n", s.c_str());
	fprintf(stdout, "\tx: %f | %f %f | %f %f %f \n",
		parameters[0], parameters[2], parameters[3],
		parameters[6], parameters[7], parameters[8]);
	fprintf(stdout, "\ty: %f | %f %f | %f %f %f \n",
		parameters[1], parameters[4], parameters[5],
		parameters[9], parameters[10], parameters[11]);

      }

      //
      pos_row[0] = 0; pos_col[0] = 0;
      if (model_inv.getDisplacement(pos_row[0], pos_col[0],
				    dep_row[0], dep_col[0]) == true)
	cout << "Backwarp pos 1 (" << pos_row[0] << "," << pos_col[0]
	     << "): " << dep_row[0] << "," << dep_col[0] << endl;
      //
      pos_row[1] = 0; pos_col[1] = I.GetCols()-1;
      if (model_inv.getDisplacement(pos_row[1], pos_col[1],
				    dep_row[1], dep_col[1]) == true)
	cout << "Backwarp pos 2 (" << pos_row[1] << "," << pos_col[1]
	     << "): " << dep_row[1] << "," << dep_col[1] << endl;
      //
      pos_row[2] = I.GetRows() - 1; pos_col[2] = 0;
      if (model_inv.getDisplacement(pos_row[2], pos_col[2],
				    dep_row[2], dep_col[2]) == true)
	cout << "Backwarp pos 3 (" << pos_row[2] << "," << pos_col[2]
	     << "): " << dep_row[2] << "," << dep_col[2] << endl;

	//
      pos_row[3] = I.GetRows() - 1; pos_col[3] = I.GetCols()-1;
      if (model_inv.getDisplacement(pos_row[3], pos_col[3],
				    dep_row[3], dep_col[3]) == true)
	cout << "Backwarp pos 4 (" << pos_row[3] << "," << pos_col[3]
	     << "): " << dep_row[3] << "," << dep_col[3] << endl;
    }

      // Construct the backwarped image filename
    filename = setfilename(bpath, frame, ndigits_out, ext);

    if (verbose)
      cout << "\tWrite backwarped image: " << filename.c_str() << endl ;

    switch (format) {
    case PGM:
      // Write the backwarped image in PGM P5 format
      ierr = WritePGM(B, filename.c_str());
      break;
    case PPM:
      // Write the backwarped image in PPM P6 format
      ierr = WritePPM(B, filename.c_str());
      break;
#ifndef __NO_IMAGEIO_PNG_
    case PNG:
      // Write the backwarped image in PNG format
      ierr = WritePNG(B, filename.c_str());
      break;
#endif
    }
    if (ierr == false) exit(-1);
  }

  // Open the result file
  if ( !rpath.empty() ) {
    rfile = fopen(rpath.c_str(), "wb");
    string s = model.idToString();

    fprintf(rfile, "# Motion2D Copyright (c) 1995-2005 by INRIA\n\
# \n\
# This file contains the parameter values of the estimated 2D \n\
# parametric motion model. A comment line starts with the # \n\
# character. After the comments, the first line refers to the \n\
# estimated model id. Next, each line refers to the motion \n\
# model parameters estimated between two successive images. \n\
# \n\
# The data signification is given below. \n\
# \n\
# |--------------------------------------------------------| \n\
# | column | data signification for each estimation        | \n\
# | number | between two successive images.                | \n\
# |--------|-----------------------------------------------| \n\
# |   1    | number of the first image                     | \n\
# |--------|-----------------------------------------------| \n\
# |   2    | motion model origin (row coordinate or yc)    | \n\
# |   3    | motion model origin (column coordinate or xc) | \n\
# |--------|-----------------------------------------------| \n\
# |   4    | motion model parameter (c1)                   | \n\
# |   5    | motion model parameter (c2)                   | \n\
# |--------|-----------------------------------------------| \n\
# |   6    | motion model parameter (a1)                   | \n\
# |   7    | motion model parameter (a2)                   | \n\
# |   8    | motion model parameter (a3)                   | \n\
# |   9    | motion model parameter (a4)                   | \n\
# |--------|-----------------------------------------------| \n");

fprintf(rfile, "\
# |  10    | motion model parameter (q1)                   | \n\
# |  11    | motion model parameter (q2)                   | \n\
# |  12    | motion model parameter (q3)                   | \n\
# |  13    | motion model parameter (q4)                   | \n\
# |  14    | motion model parameter (q5)                   | \n\
# |  15    | motion model parameter (q6)                   | \n\
# |--------|-----------------------------------------------| \n\
# |  16    | constant illumination variation parameter     | \n\
# |--------|-----------------------------------------------| \n\
# |  17    | support size (only if computed, by default)   | \n\
# |--------|-----------------------------------------------| \n\
# |  18    | exponential illumination variation parameter  | \n\
# |--------|-----------------------------------------------| \n\
#  \n%s\n", s.c_str());
  }
  // Robust estimation loop
  for (long unsigned n=1; n <= niter; n++) {

    // Initialize the support
    if (!spath.empty()) { // Otherwise the support consist of the whole image
      filename = setfilename(spath, frame, ndigits_in, ext);
      if (verbose)
	cout << "Load support: " << filename.c_str() << endl;

      switch (format) {
      case PGM:
	// Load the support image PGM P5 file
	ierr = ReadPGM(S, filename.c_str());
	break;
      case PPM:
	// Load the support image PPM P6 file
	ierr = ReadPPM(S, filename.c_str());
	break;
#ifndef __NO_IMAGEIO_PNG_
      case PNG:
	ierr = ReadPNG(S, filename.c_str()); // Load the image PNG file
	break;
#endif
      }
      if (ierr == false) exit(-1);

      // Test if the support is empty
      support_empty = true;
      for (unsigned int i=0; i < S.GetRows() * S.GetCols(); i++) {
	if (S.bitmap[i] == label) {
	  support_empty = false;
	  break;
	}
      }

      if (support_empty) {
	if (verbose)
	  cout << "\tWarning: the support is empty, motion estimation not done"
	       << endl;
      }
    }

    // Update the image frame number
    frame += step;

    // Load the next image (image 2)
    filename = setfilename(ipath, frame, ndigits_in, ext);
    if (verbose)
      cout << "Load image: " << filename.c_str() << endl ;

    switch (format) {
    case PGM:
      // Load the image PGM P5 file
      ierr = ReadPGM(I, filename.c_str());
      break;
    case PPM:
      // Load the image PPM P6 file
      ierr = ReadPPM(I, filename.c_str());
      break;
#ifndef __NO_IMAGEIO_PNG_
    case PNG:
      ierr = ReadPNG(I, filename.c_str()); // Load the image PNG file
      break;
#endif
    }
    if (ierr == false) exit(-1);

    // Fill the pyramid 2 with image 2
    pyramid2.build(I.bitmap);

    // 2D Parametric motion estimation
    if ( ! support_empty) {
      bool state;
      state = estimator.estimate(pyramid1, pyramid2, S.bitmap, label, model);
      if (state == false){
	pyramid1.destroy();
	pyramid2.destroy();
	exit(0);
      }
    }

    // Exchange pyramid 1 and 2
    pyramid1.exchange(pyramid2);

    if (support_empty) {
      continue;
    }

    // Print results
    double support_size;
    if (! estimator.getSupportSize(support_size)) support_size = -1.;
    if (verbose) {
      printresults(stdout, frame - step, frame, model, support_size);
      if ( estimator.isResidualVarianceComputed() )
	cout << "\tResidual variance: "
	     << estimator.getResidualVariance() << endl;

      if ( estimator.isCovarianceMatrixComputed() ) {
	int cov_nrows, cov_ncols;
	double *cov = estimator.getCovarianceMatrix(cov_nrows, cov_ncols);
	cout << "\tCovariance matrix: " <<  endl;
	for (int i=0; i < cov_nrows; i ++) {
	  cout << "\t";
	  for (int j=0; j < cov_ncols; j ++) {
	    fprintf(stdout, "%9.3g ", cov[i*cov_ncols + j]);
	    //cout << cov[i*cov_ncols + j] << " ";
	  }
	  cout << endl;
	}
      }
    }

    if ( !rpath.empty() ) {
      if (verbose)
	cout << "\tUpdate the result file: " << rpath.c_str() << endl;
      printresults(rfile, frame - step, frame, model, support_size);
    }

    if ( !bpath.empty() ) {
      // Backwarp the current image in the first image reference frame
      warping.backWarp(I.bitmap, I.GetRows(), I.GetCols(), B.bitmap, model);
      {
	CMotion2DModel model_inv;
	if (model.invert(model_inv) == true)
	{
	  double row, col;
	  double parameters[CMotion2DModel::MDL_NMAX_COEF];
	  model_inv.getOrigin(row, col);
	  model_inv.getParameters(parameters);

	  // Print the motion estimation results
	  fprintf(stdout, "\tinverted model \n\t");

	  fprintf(stdout, "origin: %f %f\n\t", row, col);

	  string s = model_inv.idToString();
	  fprintf(stdout, "motion model: %s\n", s.c_str());
	  fprintf(stdout, "\tx: %f | %f %f | %f %f %f \n",
		  parameters[0], parameters[2], parameters[3],
		  parameters[6], parameters[7], parameters[8]);
	  fprintf(stdout, "\ty: %f | %f %f | %f %f %f \n",
		  parameters[1], parameters[4], parameters[5],
		  parameters[9], parameters[10], parameters[11]);

	}

	//
	pos_row[0] = 0; pos_col[0] = 0;
	model_inv.getDisplacement(pos_row[0], pos_col[0],
				dep_row[0], dep_col[0]);
	cout << "Backwarp pos 1 (" << pos_row[0] << "," << pos_col[0] << "): "
	     << dep_row[0] << "," << dep_col[0] << endl;
	//
	pos_row[1] = 0; pos_col[1] = I.GetCols()-1;
	model_inv.getDisplacement(pos_row[1], pos_col[1],
				dep_row[1], dep_col[1]);
	cout << "Backwarp pos 2 (" << pos_row[1] << "," << pos_col[1] << "): "
	     << dep_row[1] << "," << dep_col[1] << endl;
	//
	pos_row[2] = I.GetRows() - 1; pos_col[2] = 0;
	model_inv.getDisplacement(pos_row[2], pos_col[2],
				dep_row[2], dep_col[2]);
	cout << "Backwarp pos 3 (" << pos_row[2] << "," << pos_col[2] << "): "
	     << dep_row[2] << "," << dep_col[2] << endl;

	//
	pos_row[3] = I.GetRows() - 1; pos_col[3] = I.GetCols()-1;
	model_inv.getDisplacement(pos_row[3], pos_col[3],
				dep_row[3], dep_col[3]);
	cout << "Backwarp pos 4 (" << pos_row[3] << "," << pos_col[3] << "): "
	     << dep_row[3] << "," << dep_col[3] << endl;
      }

      // Construct the backwarped image filename
      filename = setfilename(bpath, frame, ndigits_out, ext);

      if (verbose)
	cout << "\tWrite backwarped image: " << filename.c_str() << endl ;

      switch (format) {
      case PGM:
	// Write the backwarped image in PGM P5 format
	ierr = WritePGM(B, filename.c_str());
	break;
      case PPM:
	// Write the backwarped image in PGM P6 format
	ierr = WritePPM(B, filename.c_str());
	break;
#ifndef __NO_IMAGEIO_PNG_
      case PNG:
	// Write the backwarped image in PNG format
	ierr = WritePNG(B, filename.c_str());
	break;
#endif
      }
      if (ierr == false) exit(-1);

    }

    if ( !wpath.empty() ) {
      float *weights = NULL;
      int n_rows = 0, n_cols = 0;
      weights = estimator.getWeights(n_rows, n_cols); // weights are in [0,1]
      W.Init(n_rows, n_cols);
      for (int i=0; i < n_rows; i++)
	for (int j=0; j < n_cols; j++)
	  W[i][j] = (unsigned char) (weights[i*n_cols + j] * 255);

      // Construct the M-estimator weights image filename
      filename = setfilename(wpath, frame - step, ndigits_out, ext);

      if (verbose)
	cout << "\tWrite M-estimator weights image: "
	     << filename.c_str() << endl;

      switch (format) {
      case PGM:
	// Write the M-estimator weights image in PGM P5 format
	ierr = WritePGM(W, filename.c_str());
	break;
      case PPM:
	// Write the M-estimator weights image in PGM P6 format
	ierr = WritePPM(W, filename.c_str());
	break;
#ifndef __NO_IMAGEIO_PNG_
      case PNG:
	// Write the M-estimator weights image in PNG format
	ierr = WritePNG(W, filename.c_str());
	break;
#endif
      }
      if (ierr == false) exit(-1);
    }
  }

  // Destruction des pyramides
  pyramid1.destroy();
  pyramid2.destroy();

  if ( !rpath.empty() )
    fclose(rfile);

  return 0;
}

/*
  Build the filename of an image.
*/
string setfilename(string ipath, long frame,
		   unsigned ndigits, string ext)
{
  char buf[255];
  sprintf(buf, "%ld", frame);
  string zeros;
  zeros += "0000000000";
  zeros += buf;

  unsigned lng = max(ndigits, strlen(buf));

  string filename;
  filename += ipath;
  filename += zeros.substr(zeros.size()-lng, lng);
  filename += ext;

  return filename;
}

/*
  Print results.

*/
void printresults(FILE *output, unsigned long i1, unsigned long i2,
		  CMotion2DModel model, double support_size)
{

  double row, col;
  double parameters[CMotion2DModel::MDL_NMAX_COEF];
  double const_var_light, exp_var_light;
  model.getOrigin(row, col);
  model.getParameters(parameters);

  if (output == stdout) {
    // Print the motion estimation results
    fprintf(output, "\t2D estimated model between images %lu and %lu:\n\t",
	    i1, i2);

    fprintf(output, "origin: %f %f\n\t", row, col);

    string s = model.idToString();
    fprintf(output, "motion model: %s\n", s.c_str());
    fprintf(output, "\tx: %f | %f %f | %f %f %f \n",
	    parameters[0], parameters[2], parameters[3],
	    parameters[6], parameters[7], parameters[8]);
    fprintf(output, "\ty: %f | %f %f | %f %f %f \n",
	    parameters[1], parameters[4], parameters[5],
	    parameters[9], parameters[10], parameters[11]);

    if (model.getConstVarLight(const_var_light))
      fprintf(output, "\tconstant lobal illumination: %f \n", const_var_light);
    if (model.getExpVarLight(exp_var_light))
      fprintf(output, "\texp global illumination: %f \n", exp_var_light);

    if (support_size != -1.)
      fprintf(output, "\tsupport size: %f \n", support_size);
  }
  else {
    fprintf(output, "%lu %f %f ", i1, row, col);
    for (int i = 0; i < CMotion2DModel::MDL_NMAX_COEF; i++)
      fprintf(output, "%f ", parameters[i]);
    if (!model.getConstVarLight(const_var_light)) const_var_light = 0;
      fprintf(output, "%f ", const_var_light);
    if (support_size != -1.)
      fprintf(output, "%f ", support_size);
    if (!model.getExpVarLight(exp_var_light)) exp_var_light = 0;
      fprintf(output, "%f ", exp_var_light);
    fprintf(output, "\n");
  }
  fflush(output);
}

/*

  Print the program options.

 */
void usage(char *name, char *badparam)
{
  if (badparam)
    fprintf(stderr, "\nBad parameter [%s]\n", badparam);

  fprintf(stdout, "\n\
  Copyright (c) 1995-2005 by INRIA.\n\
  All Rights Reserved.\n\
\n\
  This software was developed at:\n\
  IRISA/INRIA Rennes\n\
  Campus Universitaire de Beaulieu \n\
  35042 Rennes Cedex\n\
\n\
  http://www.irisa.fr\n\
\n\
SYNOPSIS\n\
  %s [-p image_path] [-e image_extension] [-f first_frame]\n\
  [-s step] [-i iterations] [-m model_id] [-g] [-x model_col_orig]\n\
  [-y model_row_orig] [-a support_path] [-z] [-c support_label] \n\
  [-n number_of_pyramid_levels] [-l pyramid_stop_level]\n\
  [-r results_filename] [-b back_warped_image_path] \n\
  [-j back_warped_image_nrows] [-k back_warped_image_ncols] \n\
  [-t back_warped_image_row_origin] [-u back_warped_image_col_origin] \n\
  [-w weights_path] [-v] [-h] [-?]\n\
\n\
DESCRIPTION\n\
  The Motion2D software provides a method to estimate 2D parametric\n\
  motion models between two successive images. It can handle several\n\
  types of motion models, respectively, constant model (translation),\n\
  affine, and quadratic models. Moreover, it integrates the possibi-\n\
  lity of taking into account the global variation of illumination.\n\
  Motivations for the use of such models are, on one hand, their\n\
  efficiency, which has been demonstrated in numerous contexts such\n\
  as estimation, segmentation, tracking, and interpretation of motion, \n\
  and on the other hand, their low conputational cost compared to \n\
  optical flow estimation. Moreover to have the best accuracy for the\n\
  estimated parameters, and to take into account the problem of mul-\n\
  tiple motion, Motion2D exploit a robust, multiresolution and incre-\n\
  mental estimation method exploiting only the spatio-temporal deri-\n\
  vatives of the intensity function.\n\
\n", name);

#ifndef __NO_IMAGEIO_PNG_
  fprintf(stdout, "\n\
INPUT SEQUENCE OPTIONS:					      Default\n\
\n\
  -p image_path [%%s]                  ../../test/sequence/rond-point\n\
     Specify the path and the generic name of the files \n\
     containing the images to process. The following image\n\
     file formats PNG and PNM are supported. The different\n\
     PNM formats are PGM (P5) and PPM (P6).\n\
\n\
  -e image_extension [%%s]                                       .pgm\n\
     Specify the image extension. Supported image formats \n\
     are PNG (use -e .png), PGM P5 (use -e .pgm) and PPM P6\n\
     (use -e .ppm). \n\
\n");
#else
  fprintf(stdout, "\n\
INPUT SEQUENCE OPTIONS:					      Default\n\
\n\
  -p image_path [%%s]                  ../../test/sequence/rond-point\n\
     Specify the path and the generic name of the files \n\
     containing the images to process. Only the PNM formats \n\
     PGM (P5) and PPM (P6) are supported.\n\
\n\
  -e image_extension [%%s]                                       .pgm\n\
     Specify the image extension. Supported image formats \n\
     are PGM P5 (use -e .pgm) and PPM P6 (use -e .ppm). \n\
\n");
#endif
  fprintf(stdout, "\
  -f first_frame [%%s]                                           0001\n\
     Specify the number of the first frame in the video \n\
     sequence. If the image sequence numbering uses a fixed \n\
     number of digits, complete whith 0 before the image \n\
     number.\n\
\n\
  -s step [%%d]                                                     1\n\
     Specify the step between two frames in the video sequence.\n\
     If step > 0 images are processed forward. If step < 0 \n\
     images are processed backward. This parameter allow the \n\
     video temporal subsampling.\n\
\n\
  -i iterations [%%lu]                                             33\n\
     Specify the number of motion estimation iterations to\n\
     process. The number of the last image computed is given by:\n\
     first_frame + iterations * step.\n\
\n");

  fprintf(stdout, "\n\
MOTION MODEL OPTIONS (input):\n\
\n\
  -m model_id [%%s]                                                AC\n\
     Specify the parametric motion model id to estimate. The\n\
     table below gives the list of possible model_id strings: \n\
     |-----------------------------------------------------| \n\
     | model_id | type        | number of parameters       | \n\
     |-----------------------------------------------------| \n\
     | TX       | constant    |  1 (c1)                    | \n\
     | TY       | constant    |  1 (c2)                    | \n\
     | TR       | constant    |  2 (c1,c2)                 | \n\
     | ----------------------------------------------------| \n\
     | AXD      | affine      |  2 (c1,a1)                 | \n\
     | ARD      | affine      |  2 (c1,c2,a1)              | \n\
     | ARR      | affine      |  3 (c1,c2,a1)              | \n\
     | AXN      | affine      |  5 (c2,a1...a4)            | \n\
     | AYN      | affine      |  5 (c1,a1...a4)            | \n\
     | ADN      | affine      |  5 (c1,c2,a1,a2,a3         | \n\
     | ARN      | affine      |  5 (c1,c2,a1,a2,a4)        | \n\
     | AH1N     | affine      |  5 (c1,c2,a1,a2,a3)        | \n\
     | AH2N     | affine      |  5 (c1,c2,a1,a2,a4)        | \n\
     | ARRD     | affine      |  4 (c1,c2,a1,a2)           | \n\
     | AC       | affine      |  6 (c1,c2,a1...a4)         | \n\
     | ----------------------------------------------------| \n\
     | QPD      | quadratic   |  4 (c1,a1,q1,q2)           | \n\
     | QPT      | quadratic   |  4 (c1,c2,q1,q2)           | \n\
     | QPTD     | quadratic   |  5 (c1,c2,a1,q1,q2)        | \n\
     | Q2D      | quadratic   |  8 (c1,c2,a1...a4,q1,q2)   | \n\
     | QC       | quadratic   | 12 (c1,c2,a1...a4,q1...q6) | \n\
     ------------------------------------------------------| \n");
  fprintf(stdout, "\
     | MDL_TX               | same as TX                   | \n\
     | MDL_TY               | same as TX                   | \n\
     | MDL_TR               | same as TR                   | \n\
     | MDL_AFF_TX_DIV       | same as AXD                  | \n\
     | MDL_AFF_TR_DIV       | same as ARD                  | \n\
     | MDL_AFF_TR_ROT       | same as ARR                  | \n\
     | MDL_AFF_TX_NULL      | same as AXN                  | \n\
     | MDL_AFF_TY_NULL      | same as AYN                  | \n\
     | MDL_AFF_DIV_NULL     | same as ADN                  | \n\
     | MDL_AFF_ROT_NULL     | same as ARN                  | \n\
     | MDL_AFF_HYP1_NULL    | same as AH1N                 | \n\
     | MDL_AFF_HYP2_NULL    | same as AH2N                 | \n\
     | MDL_AFF_TR_ROT_DIV   | same as ARRD                 | \n\
     | MDL_AFF_COMPLET      | same as AC                   | \n\
     | MDL_QUA_PAN_DIV      | same as QPD                  | \n\
     | MDL_QUA_PAN_TILT     | same as QPT                  | \n\
     | MDL_QUA_PAN_TILT_DIV | same as QPTD                 | \n\
     | MDL_QUA_2D           | same as Q2D                  | \n\
     | MDL_QUA_COMPLET      | same as QC                   | \n\
     ------------------------------------------------------| \n\
\n\
  -g\n\
     Specify that the global illumination constant parameter will be\n\
     estimated.\n\
\n\
  -G\n\
     Specify that the global illumination exponential parameter will be\n\
     estimated.\n\
                \n\
  -x model_col_orig [%%f]\n\
     Sets the origin (column coordinate) of the motion model.\n\
     By default, this parameter is initialized to the mid image\n\
     column number.\n\
\n\
  -y model_row_orig [%%f]\n\
     Sets the origin (row coordinate) of the motion model.\n\
     By default, this parameter is initialized to the mid image\n\
     line number.\n\
\n\
  -z \n\
     Computes the covariance matrix of the motion model parameters.\n\
\n");

  fprintf(stdout, "\n\
ESTIMATION SUPPORT OPTIONS (input):\n\
\n\
  -a support_path [%%s]                                 \n\
     Motion2D makes it possible to estimate motion either on \n\
     all the image (by defect), or on a part of the image. In \n\
     this case, it is necessary to indicate to the software the\n\
     position of the area on which the motion estimation must be \n\
     done. This is carried out while associating each image of \n\
     the video sequence a file called estimation support, corres-\n\
     ponding to an image with same size, numbering and extension \n\
     as the images to be treated. Thus, this option specify the \n\
     path and the generic name of the files containing the images \n\
     corresponding to the estimation support. The images format \n\
     depends on the treated image extension specified by \n\
     option -e. For example, to estimate the motion between \n\
     images \"rond-point0001.pgm\" and \"rond-point0002.pgm\", the\n\
     support file name must be \"support0001.pgm\".\n\
\n\
  -c support_label [%%d] \n\
     This option is associated to the previous one. It fixes the\n\
     value of the estimation support label where the estimation \n\
     will be achieved. \n\
\n");

  fprintf(stdout, "\n\
MULTI-RESOLUTION FRAMEWORK OPTIONS (input):\n\
\n\
  -n number_of_pyramid_levels [%%u]                                 4\n\
     Specify the number of levels in the Gaussian image pyramids\n\
     and image gradients pyramids used in the multi-resolution\n\
     framework. In general, when the size of the images to be\n\
     treated is close to CIF format (352 X 288), it is advised\n\
     to fix this parameter at 4. When the images are with QCIF\n\
     format (176 X 144), this parameter can be fixed at 3.\n\
     This parameter implicitly fix the index of the initial \n\
     level in the pyramids where the robust estimate begins. This \n\
     initial level is than equal to the number of levels in the \n\
     pyramids minus one. The selected level is then that of lower\n\
     resolution.\n\
\n\
  -l pyramid_stop_level [%%u]                                       0\n\
     Specify the level of the image pyramids where the estimation\n\
     process will be stopped. This parameter in general fixed at\n\
     zero (corresponds then to the level of higher resolution) \n\
     can vary in the range [ 0, number_of_pyramid_levels - 1]. \n\
     The coarsest level is given by number_of_pyramid_levels - 1.  \n\
     The finest level is 0.\n\
\n");

  fprintf(stdout, "\n\
RESULTS OPTIONS (output):\n\
\n\
  -r results_filename [%%s]                       \n\
     Specify the name of the file which will contain the \n\
     estimated parametric motion model results.	This result file\n\
     contains the values of the estimated 2D parametric motion \n\
     model.\n\
\n\
  -b back_warped_image_path [%%s]                           \n\
     Specify the path and the generic name of the files contain-\n\
     ing the back-warped images built using the estimated motion\n\
     model. The generated images format depends on the treated  \n\
     image extension specified by option -e.\n\
\n\
  -j back_warped_image_nrows [%%d]                           \n\
     Set the number of rows of the back-warped image.\n\
     By default, the back-warped image has the same rows number\n\
     than the images to proceed. This option is taken into \n\
     account only when option -b is used.\n\
\n\
  -k back_warped_image_ncols [%%d]                           \n\
     Set the number of columns of the back-warped image.\n\
     By default, the back-warped image has the same rows number\n\
     than the images to proceed. This option is taken into \n\
     account only when option -b is used.\n\
\n");

   fprintf(stdout, "\n\
  -t back_warped_image_row_origin [%%d]                             0\n\
     This option makes it possible to fix the co-ordinates of the \n\
     row origin of the back-warped image compared to the origin \n\
     of the images to treat. This option is taken into account \n\
     only when option -b is used.\n\
\n\
  -u back_warped_image_col_origin [%%d]                             0\n\
     This option makes it possible to fix the co-ordinates of the \n\
     column origin of the back-warped image compared to the origin \n\
     of the images to treat. This option is taken into account \n\
     only when option -b is used.\n\
\n\
  -w weights_path [%%s]                           \n\
     Specify the path and the generic name of the files contain-\n\
     ing the M-estimator weights. These weights in [0,1] are\n\
     rescaled in [0,255]. This map can be used to see\n\
     if a pixel participate to the robust motion estimation \n\
     (pixel in white) or is more considered as an outlier \n\
     (pixel in black). The generated images format depends on the \n\
     treated image extension specified by option -e.\n\
\n\
OTHER OPTIONS:\n\
\n\
  -v\n\
     Activate the verbose mode.\n\
\n\
  -h\n\
     Print the help.\n\
\n\
  -?\n\
     Print the help.\n\
\n\
\n");

  exit(0);
}

/*

  Set the program options.

*/
void getoptions(int argc, char **argv,
		string &ipath, string &ext,
		string &first, int &step, long unsigned &niter,
		string &model_id, bool &const_var_light, bool &exp_var_light,
		bool &model_orig_fixed,
		double &model_row_orig, double &model_col_orig,
		unsigned &pyr_nlevels, unsigned &pyr_stop_level,
		bool &compute_covariance,
		string &rpath, string &bpath,
		int &b_nrows, int &b_ncols, int &b_row_orig, int &b_col_orig,
		string &wpath, string &spath, int &slabel, bool &verbose)
{
  char *optarg;
  int	c;
  while ((c = getoption(argc, argv, (char *)GETOPTARGS, &optarg)) > 1) {

    switch (c) {
    case 'a': spath = optarg; break;
    case 'b': bpath = optarg; break;
    case 'c': slabel = atoi(optarg); break;
    case 'e': ext = optarg; break;
    case 'f': first = optarg; break;
    case 'g': const_var_light = true; break;
    case 'G': exp_var_light = true; break;
    case 'h': usage(argv[0], NULL); break;
    case 'i': niter = atoi(optarg); break;
    case 'j': b_nrows = atoi(optarg); break;
    case 'k': b_ncols = atoi(optarg); break;
    case 'l': pyr_stop_level = atoi(optarg); break;
    case 'm': model_id = optarg; break;
    case 'n': pyr_nlevels = atoi(optarg); break;
    case 'p': ipath = optarg; break;
    case 'r': rpath = optarg; break;
    case 's': step = atoi(optarg); break;
    case 't': b_row_orig = atoi(optarg); break;
    case 'u': b_col_orig = atoi(optarg); break;
    case 'v': verbose = true; break;
    case 'x': model_orig_fixed = true; model_col_orig = atoi(optarg); break;
    case 'y': model_orig_fixed = true; model_row_orig = atoi(optarg); break;
    case 'w': wpath = optarg; break;
    case 'z': compute_covariance = true; break;
    case '?': usage(argv[0], NULL); break;

    default:  usage(argv[0], NULL); break;
    }
  }

  if ((c == 1) || (c == -1)) {
    // standalone param or error
    fprintf(stderr, "Bad argument %s\n", optarg);
    exit(0);
  }

  // Some option tests
  if ( !rpath.empty() ) {
    FILE *rfile = fopen(rpath.c_str(), "wb");
    if (rfile == NULL) {
      cout << endl
	   << "Bad argument -r: "
	   << "Cannot create the result filename specified "
	   << endl
	   << "with this option."
	   << endl;
      exit(0);
    }
    fclose(rfile);
  }

  if ( !spath.empty() ) {
    // Test if the support label is set
    if (slabel == -1) {
      cout << endl
	   << "Error: When using option -a to specify a support image path, "
	   << endl
	   << "you have to specify the support label too with option -c"
	   << endl;
      exit(0);
    }
  }

  if (slabel != -1) {
    // Test if the support image path is set
    if ( spath.empty() ) {
      cout << endl
	   << "Error: When using option -c to specify a support label, "
	   << endl
	   << "you have to specify the support image path too with option -a"
	   << endl;
      exit(0);
    }
  }

  if (pyr_nlevels < 1) {
    // The pyramids can not be build
    cout << endl
	 << "Error: When using option -n to specify the number of levels"
	 << endl
	 << "in the pyramids. This number must be greater than zero. "
	 << endl;
    exit(0);
  }

  if ( bpath.empty() ) {
    bool ommit = false;
    if (b_nrows != -1) {
      cout << "Warning: option -j ignored. " << endl;
      ommit = true;
    }
    if (b_ncols != -1) {
      cout << "Warning: option -k ignored. " << endl;
      ommit = true;
    }
    if (b_row_orig != 0) {
      cout << "Warning: option -t ignored. " << endl;
      ommit = true;
    }
    if (b_col_orig != 0) {
      cout << "Warning: option -u ignored. " << endl;
      ommit = true;
    }
    if (ommit == true)
      cout << "To take it into account don't ommit option -b." << endl << endl;
  }
}

/*

  Get next command line option and parameter

  PARAMETERS:

      argc - count of command line arguments
      argv - array of command line argument strings
      pszValidOpts - string of valid, case-sensitive option characters,
                     a colon ':' following a given character means that
                     option can take a parameter
      ppszParam - pointer to a pointer to a string for output

  RETURNS:

      If valid option is found, the character value of that option
          is returned, and *ppszParam points to the parameter if given,
          or is NULL if no param
      If standalone parameter (with no option) is found, 1 is returned,
          and *ppszParam points to the standalone parameter
      If option is found, but it is not in the list of valid options,
          -1 is returned, and *ppszParam points to the invalid argument
      When end of argument list is reached, 0 is returned, and
          *ppszParam is NULL
*/
int getoption (
    int argc,
    char** argv,
    char* pszValidOpts,
    char** ppszParam)
{
  static int iArg = 1;
  int chOpt;
  char* psz = NULL;
  char* pszParam = NULL;

  if (iArg < argc) {
    psz = &(argv[iArg][0]);
    if (*psz == '-') { // || *psz == '/')  {
      // we have an option specifier
      chOpt = argv[iArg][1];
      if (isalnum(chOpt) || ispunct(chOpt)) {
	// we have an option character
	psz = strchr(pszValidOpts, chOpt);
	if (psz != NULL) {
	  // option is valid, we want to return chOpt
	  if (psz[1] == ':') {
	    // option can have a parameter
	    psz = &(argv[iArg][2]);
	    if (*psz == '\0') {
	      // must look at next argv for param
	      if (iArg+1 < argc) {
		psz = &(argv[iArg+1][0]);
		// next argv is the param
		iArg++;
		pszParam = psz;
	      }
	      else {
		// reached end of args looking for param
	      }

	    }
	    else {
	      // param is attached to option
	      pszParam = psz;
	    }
	  }
	  else {
	    // option is alone, has no parameter
	  }
	}
	else {
	  // option specified is not in list of valid options
	  chOpt = -1;
	  pszParam = &(argv[iArg][0]);
	}
      }
      else {
	// though option specifier was given, option character
	// is not alpha or was was not specified
	chOpt = -1;
	pszParam = &(argv[iArg][0]);
      }
    }
    else {
      // standalone arg given with no option specifier
      chOpt = 1;
      pszParam = &(argv[iArg][0]);
    }
  }
  else {
    // end of argument list
    chOpt = 0;
  }

  iArg++;
  *ppszParam = pszParam;
  return (chOpt);
}
