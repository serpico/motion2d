/*

  Copyright (c) 1995-2005 by INRIA.
  All Rights Reserved.

  This software was developed at:
  IRISA/INRIA Rennes
  Campus Universitaire de Beaulieu
  35042 Rennes Cedex

  http://www.irisa.fr

*/

#include <stdio.h>
#include <stdlib.h>
#ifdef __SunOS_
# include <iostream.h>
#else
# include <iostream>
#endif
#include <ctype.h>
#include <string>

// Includes lies a Motion2D
#include <CMotion2DImage.h>
#include <CMotion2DModel.h>
#include <CMotion2DWarping.h>

using namespace std;

#define GETOPTARGS	"d:e:f:hi:l:o:p:r:s:u:v?"
#define max(a,b) (a>b?a:b)

std::string setfilename(std::string ipath, long frame,
			unsigned ndigits, std::string ext);
int  getoption (int argc, char** argv, char* pszValidOpts, char** ppszParam);
void getoptions(int argc, char **argv,
		string &ipath, string &ext,
		string &first, int &step, long unsigned &niter,
		int &left, int &right, int &up, int &down,
		string &opath, bool &verbose);
void usage(char *name, char *badparam);

#define DEBUG_LEVEL1 0
#define DEBUG_LEVEL2 0


enum EImageFormat {
  PGM,
  PPM
#ifndef __NO_IMAGEIO_PNG_
  ,PNG
#endif
};

/*

  Robust multiresolution estimation of parametric motion model.

*/
int main(int argc, char **argv)
{
  CMotion2DImage<unsigned char> I;	// Input image
  CMotion2DImage<unsigned char> O;	// Output image

  std::string ipath = "../..test/sequence/rond-point"; // Image path
  std::string ext = ".pgm";		// Image extension
  std::string filename;		// Complete filename for an image of the video
  std::string first = "0001";	// First frame number to process
  std::string opath;			// Output image path
  long unsigned niter = 33;	// Number of images to process
  int  step = 1;		// Step between 2 images
  bool verbose = false;		// Verbose mode
  EImageFormat format = PGM;    // Image format (PGM, PPM,...)
  bool ierr = true;		// Image IO return value
  int left = 0, right = 0, up = 0, down = 0;

  // Read the program options
  getoptions(argc, argv,
	     ipath, ext, first, step, niter, left, right, up, down,
	     opath, verbose);

  // Initialize the parameters
  unsigned long frame  = atoi(first.c_str()); // Current frame number to process
  unsigned ndigits_in  = first.length();// Number of digits coding image number
  unsigned ndigits_out; // Number of digits coding image number
#if 0
  char last[256];
  sprintf(last, "%ld", frame + step * niter);
  ndigits_out = strlen(last);
#else
  ndigits_out = ndigits_in;
#endif

  // Set the image format by regarding the image extension
  if (ext == ".pgm")      format = PGM;
  else if (ext == ".ppm") format = PPM;
#ifndef __NO_IMAGEIO_PNG_
  else if (ext == ".png") format = PNG;
  else {
    std::cerr << "Error: Only PNG and PNM (PGM P5 and PPM P6) image format "
	 << std::endl << "are implemented." << std::endl;
    exit(-1);
  }
#else
  else {
    std::cerr << "Error: Only PNM (PGM P5 and PPM P6) image format "
	 << std::endl << "are implemented." << std::endl;
    exit(-1);
  }
#endif


  // Start the warping loop
  long unsigned i=0;
  do {

    // Load the next image (image 2)
    filename = setfilename(ipath, frame, ndigits_in, ext);
    if (verbose)
      std::cout << "Load image: " << filename.c_str() << std::endl ;

    switch (format) {
    case PGM:
      // Load the image PGM P5 file
      ierr = ReadPGM(I, filename.c_str());
      break;
    case PPM:
      // Load the image PPM P6 file
      ierr = ReadPPM(I, filename.c_str());
      break;
#ifndef __NO_IMAGEIO_PNG_
    case PNG:
      // Load the image PNG file
      ierr = ReadPNG(I, filename.c_str());
      break;
#endif
    }
    if (ierr == false) exit(-1);

    int output_rows = I.GetRows() - up - down;
    int output_cols = I.GetCols() - left - right;

    // Initialize the output image size
    O.Init(output_rows, output_cols);

    for (int _i = 0; _i < output_rows; _i ++) {
      for (int _j = 0; _j < output_cols; _j ++) {
	O[_i][_j] = I[_i+up][_j+left];
      }
    }

    // Construct the backwarped image filename
    filename = setfilename(opath, frame, ndigits_out, ext);

    if (verbose)
      std::cout << "\tWrite cropped image: " << filename.c_str() << std::endl ;

    switch (format) {
    case PGM:
      // Write the warped image in PGM P5 format
      ierr = WritePGM(O, filename.c_str());
      break;
    case PPM:
      // Write the warped image in PGM P6 format
      ierr = WritePPM(O, filename.c_str());
      break;
#ifndef __NO_IMAGEIO_PNG_
    case PNG:
      // Write the warped image in PNG format
      ierr = WritePNG(O, filename.c_str());
      break;
#endif
    }
    if (ierr == false) exit(-1);

    frame += step; // Update the image frame number

  } while (++i < niter);

  return 0;
}

/*
  Build the filename of an image.
*/
std::string setfilename(std::string ipath, long frame,
			unsigned ndigits, string ext)
{
  char buf[255];
  sprintf(buf, "%ld", frame);
  std::string zeros;
  zeros += "0000000000";
  zeros += buf;

  unsigned lng = max(ndigits, strlen(buf));

  std::string filename;
  filename += ipath;
  filename += zeros.substr(zeros.size()-lng, lng);
  filename += ext;

  return filename;
}


/*

  Print the program options.

 */
void usage(char *name, char *badparam)
{
  if (badparam)
    fprintf(stderr, "\nBad parameter [%s]\n", badparam);

  fprintf(stdout, "\n\
  Copyright (c) 1995-2005 by INRIA.\n\
  All Rights Reserved.\n\
\n\
  This software was developed at:\n\
  IRISA/INRIA Rennes\n\
  Campus Universitaire de Beaulieu \n\
  35042 Rennes Cedex\n\
\n\
  http://www.irisa.fr\n\
\n\
SYNOPSIS\n\
  %s [-p input_image_path] [-e image_extension] [-f first_frame]\n\
  [-s step] [-i iterations] [-l left] [-r right] [-u up] [-d down]\n\
  [-o cropped_image_path] [-v] [-h] [-?]\n\
\n\
DESCRIPTION\n\
  This software cropes images.\n\
\n\
\n", name);

#ifndef __NO_IMAGEIO_PNG_
  fprintf(stdout, "\
INPUT SEQUENCE OPTIONS:					      Default\n\
\n\
  -p input_image_path [%%s]            ../../test/sequence/rond-point\n\
     Specify the path and the generic name of the files \n\
     containing the images to crop. The following image\n\
     file formats PNG and PNM are supported. The different\n\
     PNM formats are PGM (P5) and PPM (P6).\n\
\n\
  -e image_extension [%%s]                                       .pgm\n\
     Specify the image extension. Supported image formats \n\
     are PNG (use -e .png), PGM P5 (use -e .pgm) and PPM P6\n\
     (use -e .ppm). \n\
\n");
#else
  fprintf(stdout, "\
INPUT SEQUENCE OPTIONS:					      Default\n\
\n\
  -p input_image_path [%%s]            ../../test/sequence/rond-point\n\
     Specify the path and the generic name of the files \n\
     containing the images to crop. Only the PNM formats \n\
     PGM (P5) and PPM (P6) are supported.\n\
\n\
  -e image_extension [%%s]                                       .pgm\n\
     Specify the image extension. Supported image formats \n\
     are PGM P5 (use -e .pgm) and PPM P6 (use -e .ppm). \n\
\n");
#endif

  fprintf(stdout, "\
  -f first_frame [%%s]                                           0001\n\
     Specify the number of the first frame in the video \n\
     sequence. If the image sequence numbering uses a fixed \n\
     number of digits, complete whith 0 before the image number.\n\
\n\
  -s step [%%d]                                                     1\n\
     Specify the step between two frames in the video sequence.\n\
     If step > 0 images are processed forward. If step < 0 images\n\
     are processed backward.\n\
\n\
  -i iterations [%%lu]                                             33\n\
     Specify the number of motion estimation iterations to\n\
     process. The number of the last image computed is given by:\n\
     first_frame + iterations * step.\n\
\n\
  -l left [%%d]                                                     0\n\
     Specify the number of columns to crop on the left image \n\
     border.\n\
\n\
  -r right [%%d]                                                    0\n\
     Specify the number of columns to crop on the right image \n\
     border.\n\
\n\
  -u up [%%d]                                                       0\n\
     Specify the number of lines to crop on the top image \n\
     border.\n\
\n\
\n\
  -d down [%%d]                                                       0\n\
     Specify the number of lines to crop on the bottom image \n\
     border.\n\
\n\
\n\
RESULTS OPTIONS:\n\
\n\
  -o cropped_image_path [%%s]                           \n\
     Specify the path and the generic name of the files contain-\n\
     ing the cropped images. The generated images format depends\n\
     on the input image extension specified by option -e.\n\
\n");

  fprintf(stdout, "\n\
OTHER OPTIONS:\n\
\n\
  -v\n\
     Activate the verbose mode.\n\
\n\
  -h\n\
     Print the help.\n\
\n\
  -?\n\
     Print the help.\n\
\n\
\n");

  exit(0);
}

/*

  Set the program options.

*/
void getoptions(int argc, char **argv,
		std::string &ipath, std::string &ext,
		std::string &first, int &step, long unsigned &niter,
		int &left, int &right, int &up, int &down,
		std::string &opath, bool &verbose)
{
  char *optarg;
  int	c;
  while ((c = getoption(argc, argv, (char *)GETOPTARGS, &optarg)) > 1) {

    switch (c) {
    case 'd': down = atoi(optarg); break;
    case 'e': ext = optarg; break;
    case 'f': first = optarg; break;
    case 'h': usage(argv[0], NULL); break;
    case 'i': niter = atoi(optarg); break;
    case 'l': left = atoi(optarg); break;
    case 'o': opath = optarg; break;
    case 'p': ipath = optarg; break;
    case 'r': right = atoi(optarg); break;
    case 's': step = atoi(optarg); break;
    case 'u': up = atoi(optarg); break;
    case 'v': verbose = true; break;
    case '?': usage(argv[0], NULL); break;

    default:  usage(argv[0], NULL); break;
    }
  }

  // Some option tests
  if (opath.empty()) {
    // standalone param or error
    fprintf(stderr, "Option -o [output_image_path] not specified\n");
    usage(argv[0], NULL);
    exit(0);
  }

  if ((c == 1) || (c == -1)) {
    // standalone param or error
    fprintf(stderr, "Bad argument %s\n", optarg);

    usage(argv[0], NULL);

    exit(0);
  }
}

/*

  Get next command line option and parameter

  PARAMETERS:

      argc - count of command line arguments
      argv - array of command line argument strings
      pszValidOpts - string of valid, case-sensitive option characters,
                     a colon ':' following a given character means that
                     option can take a parameter
      ppszParam - pointer to a pointer to a string for output

  RETURNS:

      If valid option is found, the character value of that option
          is returned, and *ppszParam points to the parameter if given,
          or is NULL if no param
      If standalone parameter (with no option) is found, 1 is returned,
          and *ppszParam points to the standalone parameter
      If option is found, but it is not in the list of valid options,
          -1 is returned, and *ppszParam points to the invalid argument
      When end of argument list is reached, 0 is returned, and
          *ppszParam is NULL
*/
int getoption (
    int argc,
    char** argv,
    char* pszValidOpts,
    char** ppszParam)
{
  static int iArg = 1;
  int chOpt;
  char* psz = NULL;
  char* pszParam = NULL;

  if (iArg < argc) {
    psz = &(argv[iArg][0]);
    if (*psz == '-') { // || *psz == '/')  {
      // we have an option specifier
      chOpt = argv[iArg][1];
      if (isalnum(chOpt) || ispunct(chOpt)) {
	// we have an option character
	psz = strchr(pszValidOpts, chOpt);
	if (psz != NULL) {
	  // option is valid, we want to return chOpt
	  if (psz[1] == ':') {
	    // option can have a parameter
	    psz = &(argv[iArg][2]);
	    if (*psz == '\0') {
	      // must look at next argv for param
	      if (iArg+1 < argc) {
		psz = &(argv[iArg+1][0]);
		// next argv is the param
		iArg++;
		pszParam = psz;
	      }
	      else {
		// reached end of args looking for param
	      }

	    }
	    else {
	      // param is attached to option
	      pszParam = psz;
	    }
	  }
	  else {
	    // option is alone, has no parameter
	  }
	}
	else {
	  // option specified is not in list of valid options
	  chOpt = -1;
	  pszParam = &(argv[iArg][0]);
	}
      }
      else {
	// though option specifier was given, option character
	// is not alpha or was was not specified
	chOpt = -1;
	pszParam = &(argv[iArg][0]);
      }
    }
    else {
      // standalone arg given with no option specifier
      chOpt = 1;
      pszParam = &(argv[iArg][0]);
    }
  }
  else {
    // end of argument list
    chOpt = 0;
  }

  iArg++;
  *ppszParam = pszParam;
  return (chOpt);
}
