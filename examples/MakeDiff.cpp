/*

  Copyright (c) 1995-2005 by INRIA.
  All Rights Reserved.

  This software was developed at:
  IRISA/INRIA Rennes
  Campus Universitaire de Beaulieu
  35042 Rennes Cedex

  http://www.irisa.fr

*/

#include <stdio.h>
#include <stdlib.h>
#ifdef __SunOS_
# include <iostream.h>
#else
# include <iostream>
#endif
#include <ctype.h>
#include <string>

// Includes lies a Motion2D
#include <CMotion2DImage.h>
#include <CMotion2DModel.h>
#include <CMotion2DWarping.h>

using namespace std;

#define GETOPTARGS	"a:b:ho:v?"
#define max(a,b) (a>b?a:b)

string setfilename(string ipath, long frame,
		   unsigned ndigits, string ext);
int  getoption (int argc, char** argv, char* pszValidOpts, char** ppszParam);
void getoptions(int argc, char **argv,
		string &ifilename1, string &ifilename2,
		string &ofilename, bool &verbose);

void usage(char *name, char *badparam);

#define DEBUG_LEVEL1 0
#define DEBUG_LEVEL2 0


/*

  Robust multiresolution estimation of parametric motion model.

*/
int main(int argc, char **argv)
{
  CMotion2DImage<unsigned char> I1;	// Input image
  CMotion2DImage<unsigned char> I2;	// Input image
  CMotion2DImage<unsigned char> O;	// Output image

  string ifilename1 = "../..test/sequence/rond-point0001.pgm"; // Image path
  string ifilename2 = "../..test/sequence/rond-point0002.pgm"; // Image path
  string ofilename;			// Output image path
  bool verbose = false;		// Verbose mode
  bool ierr = true;		// Image IO return value

  // Read the program options
  getoptions(argc, argv, ifilename1, ifilename2, ofilename, verbose);

  cout << "Read " << ifilename1 << endl;
  ierr = ReadPGM(I1, ifilename1.c_str());
  if (ierr == false) exit(-1);
  cout << "Read " << ifilename2 << endl;
  ierr = ReadPGM(I2, ifilename2.c_str());
  if (ierr == false) exit(-1);

  O.Init(I1.GetRows(), I1.GetCols());
  O = I1 - I2;

  cout << "\tWrite diff image: " << ofilename.c_str() << endl ;

  ierr = WritePGM(O, ofilename.c_str());

  return 0;
}

/*

  Print the program options.

 */
void usage(char *name, char *badparam)
{
  if (badparam)
    fprintf(stderr, "\nBad parameter [%s]\n", badparam);

  fprintf(stdout, "\n\
  Copyright (c) 1995-2005 by INRIA.\n\
  All Rights Reserved.\n\
\n\
  This software was developed at:\n\
  IRISA/INRIA Rennes\n\
  Campus Universitaire de Beaulieu \n\
  35042 Rennes Cedex\n\
\n\
  http://www.irisa.fr\n\
\n\
SYNOPSIS\n\
  %s [-a input1] [-b input2] [-o output]\n\
\n\
DESCRIPTION\n\
  This software creates the diff image\n\
\n\
\n", name);

  fprintf(stdout, "\
INPUT SEQUENCE OPTIONS:					      Default\n\
\n\
  -a input1 [%%s]            ../../test/sequence/rond-point0001.pgm\n\
     \n\
\n\
  -b input2 [%%s]            ../../test/sequence/rond-point0002.pgm\n\
     \n\
  -o output [%%s]                                       \n\
  \n\
\n");

}

/*

  Set the program options.

*/
void getoptions(int argc, char **argv,
		string &ifilename1, string &ifilename2,
		string &ofilename, bool &verbose)
{
  char *optarg;
  int	c;
  while ((c = getoption(argc, argv, (char *)GETOPTARGS, &optarg)) > 1) {

    switch (c) {
    case 'a': ifilename1 = optarg; break;
    case 'b': ifilename2 = optarg; break;
    case 'o': ofilename = optarg; break;
    case 'h': usage(argv[0], NULL); break;
    case 'v': verbose = true; break;
    case '?': usage(argv[0], NULL); break;

    default:  usage(argv[0], NULL); break;
    }
  }

  // Some option tests
  if (ofilename.empty()) {
    // standalone param or error
    fprintf(stderr, "Option -o [output] not specified\n");
    usage(argv[0], NULL);
    exit(0);
  }

  if ((c == 1) || (c == -1)) {
    // standalone param or error
    fprintf(stderr, "Bad argument %s\n", optarg);

    usage(argv[0], NULL);

    exit(0);
  }
}

/*

  Get next command line option and parameter

  PARAMETERS:

      argc - count of command line arguments
      argv - array of command line argument strings
      pszValidOpts - string of valid, case-sensitive option characters,
                     a colon ':' following a given character means that
                     option can take a parameter
      ppszParam - pointer to a pointer to a string for output

  RETURNS:

      If valid option is found, the character value of that option
          is returned, and *ppszParam points to the parameter if given,
          or is NULL if no param
      If standalone parameter (with no option) is found, 1 is returned,
          and *ppszParam points to the standalone parameter
      If option is found, but it is not in the list of valid options,
          -1 is returned, and *ppszParam points to the invalid argument
      When end of argument list is reached, 0 is returned, and
          *ppszParam is NULL
*/
int getoption (
    int argc,
    char** argv,
    char* pszValidOpts,
    char** ppszParam)
{
  static int iArg = 1;
  int chOpt;
  char* psz = NULL;
  char* pszParam = NULL;

  if (iArg < argc) {
    psz = &(argv[iArg][0]);
    if (*psz == '-') { // || *psz == '/')  {
      // we have an option specifier
      chOpt = argv[iArg][1];
      if (isalnum(chOpt) || ispunct(chOpt)) {
	// we have an option character
	psz = strchr(pszValidOpts, chOpt);
	if (psz != NULL) {
	  // option is valid, we want to return chOpt
	  if (psz[1] == ':') {
	    // option can have a parameter
	    psz = &(argv[iArg][2]);
	    if (*psz == '\0') {
	      // must look at next argv for param
	      if (iArg+1 < argc) {
		psz = &(argv[iArg+1][0]);
		// next argv is the param
		iArg++;
		pszParam = psz;
	      }
	      else {
		// reached end of args looking for param
	      }

	    }
	    else {
	      // param is attached to option
	      pszParam = psz;
	    }
	  }
	  else {
	    // option is alone, has no parameter
	  }
	}
	else {
	  // option specified is not in list of valid options
	  chOpt = -1;
	  pszParam = &(argv[iArg][0]);
	}
      }
      else {
	// though option specifier was given, option character
	// is not alpha or was was not specified
	chOpt = -1;
	pszParam = &(argv[iArg][0]);
      }
    }
    else {
      // standalone arg given with no option specifier
      chOpt = 1;
      pszParam = &(argv[iArg][0]);
    }
  }
  else {
    // end of argument list
    chOpt = 0;
  }

  iArg++;
  *ppszParam = pszParam;
  return (chOpt);
}
