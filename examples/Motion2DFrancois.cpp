#include <iostream>
#include "CMotion2D.h"

int main(int argc, char **argv)
{

  CMotion2D motion;

  motion.main(argc, argv);

#if 0
  CMotion2DModel model;
  unsigned nbsubsample;
  // debugging test
  nbsubsample = motion.getNbSubsample();
  cout << "nbsubsample: " << nbsubsample << endl;

  model = motion.getMotion2DModel();

  if (1) {
    double row, col;
    double parameters[CMotion2DModel::MDL_NMAX_COEF];
    double const_var_light;
    FILE *output = stdout;
    CMotion2DModel _model;
    _model = model.getModelLevel(0); // For the last estimation
    _model.getOrigin(row, col);
    _model.getParameters(parameters);

    // Print the motion estimation results
    fprintf(output, "\t2D estimated model:\n\t");

    fprintf(output, "origin: %f %f\n\t", row, col);

    string s = _model.idToString();
    fprintf(output, "motion model: %s\n", s.c_str());
    fprintf(output, "\tx: %f | %f %f | %f %f %f \n",
	    parameters[0], parameters[2], parameters[3],
	    parameters[6], parameters[7], parameters[8]);
    fprintf(output, "\ty: %f | %f %f | %f %f %f \n",
	    parameters[1], parameters[4], parameters[5],
	    parameters[9], parameters[10], parameters[11]);

    if (model.getConstVarLight(const_var_light))
      fprintf(output, "\tconst global illumination: %f \n", const_var_light);
    if (model.getExpVarLight(exp_var_light))
      fprintf(output, "\texp global illumination: %f \n", exp_var_light);

  }
#endif
}
