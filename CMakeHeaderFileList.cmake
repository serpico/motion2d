# Set HEADER_subdir variable to all the files we want to parse during
# the build process. 
# Don't forget to update HEADER_ALL variable if you add/remove a 
# HEADER_subdir variable
#
# If you add/remove a directory, modify here

SET (HEADER_INCLUDE
  include/CImageReader.h  
  include/CMotion2DModel.h
  include/CWriter.h
  include/CImageWriter.h
  include/CMotion2DPyramid.h
  include/FieldVector.h
  include/CMotion2D.h
  include/CMotion2DVideo_Mpeg2.h
  include/Motion2D.h
  include/CMotion2DEstimator.h
  include/CMotion2DWarping.h
  include/Motion2DImage_PNG.h
  include/CMotion2DImage.h
  include/CMpeg2Reader.h
  include/Motion2DImage_PNM.h
  include/CReader.h
  include/Motion2DImage_RAW.h
  include/Motion2DImage_TIFF.h
  include/Midway.h
  include/quantification.h
#
  src/inc/constant.h  
  src/inc/interplt.h  
  src/inc/macro.h 
  src/inc/type.h
  src/pyramide/multigr.h
  src/compense/compense.h
  src/estimation/para_mvt.h
  src/cprim/fafirf3.h  
  src/cprim/fafirf5.h  
  src/cprim/fafirf7.h  
  src/cprim/famem.h    
  src/memoire/memoire.h
  )

IF(TIFF_FOUND)
  LIST(APPEND HEADER_INCLUDE include/CImg.h)
ENDIF()

SET (HEADER_COMPENSE
  src/compense/compense.h
)
SET (HEADER_CONTRIB
  src/contrib/localpng/readpng.h 
  src/contrib/localpng/writepng.h
  src/contrib/mpeg2dec/config.h  
  src/contrib/mpeg2dec/global.h
  src/contrib/mpeg2dec/getpic.h  
  src/contrib/mpeg2dec/mpegmacro.h
  src/contrib/mpeg2dec/getvlc.h  
  src/contrib/mpeg2dec/mpeg2dec.h
  src/contrib/mpeg2dec/exttypes/dct.h  
  src/contrib/mpeg2dec/exttypes/mvectors.h
  )
SET (HEADER_CPRIM
  src/cprim/acast.h    
  src/cprim/fafirf3.h
  src/cprim/ucaarith.h
  src/cprim/daarith.h  
  src/cprim/fafirf5.h
  src/cprim/ucamem.h
  src/cprim/damem.h    
  src/cprim/fafirf7.h
  src/cprim/uiafirf3.h
  src/cprim/faarith.h  
  src/cprim/famem.h
  src/cprim/uiamem.h
  src/cprim/fafirf.h   
  src/cprim/saarith.h
  )
SET (HEADER_ESTIMATION
  src/estimation/RMRmod.h 
  src/estimation/im_spat_temp.h
  src/estimation/cog.h   
  src/estimation/irls.h
  src/estimation/covariance.h
  src/estimation/mem_est.h
  src/estimation/estimate.h 
  src/estimation/para_mvt.h
  src/estimation/estimate_aff.h
  src/estimation/variance.h
  src/estimation/estimate_const.h
  src/estimation/weights.h
  src/estimation/estimate_quad.h
  )
SET (HEADER_INC
  src/inc/constant.h
  src/inc/interplt.h
  src/inc/macro.h
  src/inc/type.h
  )
SET (HEADER_MAT_SYM
  src/mat_sym/arithm.h        
  src/mat_sym/mtx_tool.h
  src/mat_sym/inverse_mat_sym.h
  src/mat_sym/resoud_mat_sym.h
  src/mat_sym/invert.h
  )
SET (HEADER_MEMOIRE
  src/memoire/memoire.h
  )
SET (HEADER_PYRAMIDE
  src/pyramide/filt_gauss.h
  src/pyramide/multigr.h
  src/pyramide/gradient.h
  src/pyramide/pyramide.h
  )

SET (HEADER_ALL 
  ${HEADER_INCLUDE}
  ${HEADER_COMPENSE}
  ${HEADER_CONTRIB}
  ${HEADER_CPRIM}
  ${HEADER_ESTIMATION}
  ${HEADER_INC}
  ${HEADER_MAT_SYM}
  ${HEADER_MEMOIRE}
  ${HEADER_PYRAMIDE}
  )
