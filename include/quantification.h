#ifndef quantification_h
#define quantification_h

#include <CMotion2DImage.h>
#include <CImageReader.h>


// m2d core program only works on 8bits data (even if it takes short as input)
void quantify8bits (CMotion2DImage<short>& I, const unsigned short min, const unsigned short max);

// backwarping program takes short as input but does not handle signed numbers. so we quantify to have only [0..32767] values.
// I : image to quantify
// max : max value in the sequence
// min : min value in the sequence
void quantify15bits (CMotion2DImage<short>& I, const unsigned short min, const unsigned short max);

// restore image value range to [0..2^16]
// I : 15 bits image
// max : max value in the sequence
// min : min value in the sequence
void restore16bits (CMotion2DImage<short>& I, const unsigned short min, const unsigned short max);

// tell if the image use all 16 bits of the short => ie has negative value
// true if one pixel is <0
bool isImage16bits (CMotion2DImage<short>& I);

// read all images and get the min and max values
void getMinMaxSequence(CReader* Ireader, string ipath, unsigned long firstframe, int step, unsigned long niter, unsigned nbsubsample, unsigned raw_nrows, unsigned raw_ncols, unsigned short* minSequence, unsigned short* maxSequence);


#endif
