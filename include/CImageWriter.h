/*

  Copyright (c) 1995-2005 by INRIA.
  All Rights Reserved.

  This software was developed at:
  IRISA/INRIA Rennes
  Campus Universitaire de Beaulieu
  35042 Rennes Cedex

  http://www.irisa.fr

*/

/*!
  \file CImageWriter.h
  \brief File to include to use CImageWriter.
*/

#ifndef CImageWriter_h
#define CImageWriter_h

#include <CWriter.h>


class MOTION2D_EXPORT CImageWriter: public CWriter
{
 public:
  ~CImageWriter();

  string getFileName();
  bool   writeFrame(CMotion2DImage<unsigned char> & I);
  bool   writeFrame(CMotion2DImage<short> & I);
  bool   openStream() {return true;};
  bool   closeStream() {return true; };
  void   getType() { cout <<" ImageWriter "<<endl;};
  EWriterFormat getFormat();
};

#endif
