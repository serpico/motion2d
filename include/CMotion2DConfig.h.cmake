/*

  Copyright (c) 1995-2008 by INRIA.
  All Rights Reserved.

  This software was developed at:
  IRISA/INRIA Rennes
  Campus Universitaire de Beaulieu
  35042 Rennes Cedex

  http://www.irisa.fr

*/

#ifndef CMotion2DConfig_h
#define CMotion2DConfig_h

// Defined if the Key Codec is available.
#cmakedefine MOTION2D_HAVE_KEYCODEC

// Motion2D library is either compiled static or shared
// Used to set declspec(import, export) in headers if required under Windows
#cmakedefine MOTION2D_BUILD_SHARED_LIBS

// Defined if MSVC is the compiler
#cmakedefine MOTION2D_USE_MSVC

// Under Windows, for shared libraries (DLL) we need to define export on
// compilation or import on use (like a third party project).
// We exploit here the fact that cmake auto set xxx_EXPORTS (with S) on 
// compilation.
#if defined (WIN32) && defined(MOTION2D_BUILD_SHARED_LIBS) 
#  ifdef motion2d_EXPORTS 
#    define MOTION2D_EXPORT __declspec(dllexport)
#  else  
#    define MOTION2D_EXPORT __declspec(dllimport)
#  endif 
#else
#  define MOTION2D_EXPORT
#endif

#endif

