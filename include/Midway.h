#ifndef _Midway_h
#define _Midway_h

#include <string>
#include <iostream>
#include <CMotion2DImage.h>


using namespace std;


class Midway
{
public:
	Midway(void);
	~Midway(void);

	// Image equalization
	static void histogram(CMotion2DImage<short>& I, double* histo, int nb_bins, double val_max);
	static bool histogram_cumul(CMotion2DImage<short>& I, double* histo_cumul, int nb_bins, double val_max, bool normalized);
	static void midway_equalization(CMotion2DImage<short>& I1, CMotion2DImage<short>& I2);
};

#endif
