/*

  Copyright (c) 1995-2005 by INRIA.
  All Rights Reserved.

  This software was developed at:
  IRISA/INRIA Rennes
  Campus Universitaire de Beaulieu
  35042 Rennes Cedex

  http://www.irisa.fr

 */

/*!
  \file CMotion2DImage.h
  \brief File to include to use CMotion2DImage.
 */

/*!

  \class CMotion2DImage

  \brief The CMotion2DImage class provides basic image manipulations.

  <h3> Data structure </h3>

  Each image is build using two structures (an array bitmap which size
  is [nb_cols*nb_rows]) and an array of pointer rows (which size is [nb_rows])
  the \e ith element in the row array. row[i] is a pointer toward the \e ith
  "line" of the image (i.e., bitmap +i*nb_cols )

  \image html image-data-structure.gif
  \image latex image-data-structure.ps  "The image structure" width=10cm

  Such a structure allows a fast access to each element of the image.
  If \e i is the ith rows and \e j the jth column, the value of this pixel
  is given by I[i][j] (that is equivalent to row[i][j]).

  This is a template class, therefore the type of each  element of the
  array is not a priori defined.

  The CMotion2DImage class is only provided for convenience in order to build
  the Motion2D example programs. Therefore, image input/output manipulations
  are restricted and only supported for PNG (see ReadPNG() and WritePNG()
  functions), TIFF, RAW and PNM file format.

  A RAW image file contains simply the list of pixel values with no
  header. That is why the number of columns and of rows must be given by the
  user. For RAW file format images, two sets of functions exist, one for the
  traditional 8-bits images named RAW8 (see functions ReadRAW8() and
  WriteRAW8()), the other for 9 to 16 bits images named RAW16 (see functions
  ReadRAW16() and WriteRAW16()).

  The different PNM formats are PGM P5
  (see functions ReadPGM() and WritePGM()) and PPM P6 (see functions ReadPPM()
  or WritePPM()). To know more about this image file format: \e man \e png, \e
  man \e pgm or \e man \e ppm.


 */

#ifndef CMotion2DImage_h
#define CMotion2DImage_h

#ifdef __SunOS_
# include <iostream.h>
#else
# include <iostream>
#endif
#include <math.h>
#include <string.h>

#include <CMotion2DConfig.h>

using namespace std;


#ifndef FILENAME_MAX
#  define FILENAME_MAX 1024
#endif


#define DEBUG_LEVEL1 0

/*!
  Sort in an array.

  \param ra The array to sort.
  \param n Array size

  \return false if the array size is null, true if the sort was successfully
  achieved.

 */
template <class T> bool sort(unsigned n, T* ra)
{
	unsigned long l,j,ir,i;
	T rra;
	unsigned long min,max;

	if (DEBUG_LEVEL1) cout << "Begin sort()" << endl;
	if (n == 0)
		return false;

	if (n == 1)
		return true;

	min=n+1;
	max= 0; // -1 Modif FS 13/05/04
	l=(n >> 1)+1;
	ir=n;
	for (;;) {
		if (l > 1)
			rra=ra[--l];
		else {
			rra=ra[ir];
			ra[ir]=ra[1];
			if (ir>max) max=ir;
			if (ir<min) min=ir;
			if (--ir == 1) {
				ra[1]=rra;

				return true;
			}
		}
		i=l;
		j=l << 1;
		while (j <= ir) {
			if (j+1>max) max=j+1;
			if (j<min) min=j;
			if (j < ir && ra[j] < ra[j+1]) ++j;
			if (rra < ra[j]) {
				if (i>max) max=i;
				if (i<min) min=i;
				ra[i]=ra[j];
				j += (i=j);
			}
			else j=ir+1;
		}
		if (i>max) max=i;
		if (i<min) min=i;
		ra[i]=rra;
	}
	if (DEBUG_LEVEL1) cout << "End sort()" << endl;
	return true;
}




template<class Type>
class CMotion2DImage
{
private:
	unsigned npixels;
	unsigned ncols ;
	unsigned nrows ;

public:
	Type *bitmap ; /*!< The bitmap array associated to the image. */
	Type **row ; /*!< The row array to access to the first element of a line. */

public:
	CMotion2DImage() ;
	//copy constructors
	CMotion2DImage(const CMotion2DImage<Type>&);
	CMotion2DImage(unsigned nb_rows, unsigned nb_cols);
	CMotion2DImage(unsigned nb_rows, unsigned nb_cols, Type value);
	~CMotion2DImage() ;

	bool Init(unsigned nb_rows, unsigned nb_cols);
	bool Init(unsigned nb_rows, unsigned nb_cols, Type value);

	bool Resize(unsigned nb_rows, unsigned nb_cols);
	void Subsample();
	void printUnsupportedImageError();
	bool And(CMotion2DImage<Type> &A, CMotion2DImage<Type> &B);
	bool And(CMotion2DImage<Type> &A, CMotion2DImage<Type> &B, Type label);
	bool MedianFilter(unsigned filterRowSize, unsigned filterColSize);

	/*!
    Returns the number of rows of an image.

    \sa GetCols()
	 */
	inline unsigned GetRows() const { return nrows ; }
	/*!
    Returns the number of columns of an image.

    \sa GetRows()
	 */
	inline unsigned GetCols() const { return ncols ; }
	/*!

    Returns the number of pixels in the image. This number reflects the image
    size.

	 */
	inline unsigned GetNumberOfPixel() const{ return npixels; }

	/*!

    Allows a direct access to the bitmap to change the value of the pixel
    located at the \e ith line and \e jth column: [i][j] = (row[i])[j].

	 */
	Type *operator[](unsigned i) {return row[i];}
	/*!

    Allows a direct access to the bitmap to get the value of the pixel
    located at the \e ith line and \e jth column: [i][j] = (row[i])[j].

	 */
	const  Type *operator[](unsigned i) const {return row[i];}


	void operator=(Type x);

	void operator=(const CMotion2DImage<Type> &image);

	CMotion2DImage<Type> operator-(const CMotion2DImage<Type> &image);

private:
	/*!
    Set the number of columns of an image.

    \sa SetRows(), CMotion2DImage(unsigned, unsigned)
	 */
	void SetCols(unsigned nb_cols) { ncols = nb_cols ; } ;
	/*!
    Set the number of rows of an image.

    \sa SetCols(), CMotion2DImage(unsigned, unsigned)
	 */
	void SetRows(unsigned nb_rows) { nrows = nb_rows ; } ;


} ;

/*!

  Default constructor. No memory allocation is done.

  \sa CMotion2DImage(unsigned, unsigned), CMotion2DImage(unsigned, unsigned, Type),

 */
template<class Type>
CMotion2DImage<Type>::CMotion2DImage()
{
	bitmap = NULL ;
	row = NULL ;
	nrows = ncols = 0 ;
	npixels = 0;
}

/*!

  Copy constructor.

 */
template<class Type>
CMotion2DImage<Type>::CMotion2DImage(const CMotion2DImage<Type>& I)
{
	bitmap = NULL ;
	row = NULL ;
	nrows = ncols = 0 ;
	npixels = 0;

	Resize(I.GetRows(),I.GetCols());
	unsigned i;
	memcpy(bitmap, I.bitmap, I.npixels*sizeof(Type)) ;
	for (i =0  ; i < nrows ; i++) row[i] = bitmap + i*ncols ;
}

/*!

  Constructor.

  Allocates memory for an image of \e nb_rows rows and \e nb_cols columns and
  set all the elements of the bitmap to 0.

  If the image was already initialized, memory allocation is done
  only if the new image size is different, else we re-use the same
  memory space.

  \sa CMotion2DImage(), Init(unsigned, unsigned)

 */
template<class Type>
CMotion2DImage<Type>::CMotion2DImage(unsigned nb_rows, unsigned nb_cols)
{
	bitmap = NULL ;
	row = NULL ;
	nrows = ncols = 0 ;
	npixels = 0;

	Init(nb_rows,nb_cols,0) ;
}

/*!

  Constructor.

  Allocates memory for an image of \e nb_rows rows and \e nb_cols columns and
  set all the elements of the bitmap to \e value.

  If the image was already initialized, memory allocation is done
  only if the new image size is different, else we re-use the same
  memory space.

  \sa CMotion2DImage(), Init(unsigned, unsigned)

 */
template<class Type>
CMotion2DImage<Type>::CMotion2DImage (unsigned nb_rows, unsigned nb_cols, Type value)
{
	bitmap = NULL ;
	row = NULL ;
	nrows = ncols = 0 ;
	Init(nb_rows,nb_cols,value) ;
}

/*!

  Destroys the image. Free the memory used by the image.

 */
template<class Type>
CMotion2DImage<Type>::~CMotion2DImage()
{
	if (row!=NULL) {
		delete [] row ;
		row = NULL;
	}
	if (bitmap!=NULL) {
		delete [] bitmap;
		bitmap = NULL;
	}
}




/*!

  Allocates memory for an image of \e nb_rows rows and \e nb_cols
  columns. Elements of the bitmap are not initialized.

  If the image was already initialized, memory allocation is done
  only if the new image size is different, else we re-use the same
  memory space.

  \return true if successful, or false if memory cannot be allocated.

  \sa Init(unsigned, unsigned, Type)

 */
template<class Type>
bool
CMotion2DImage<Type>::Init(unsigned nb_rows, unsigned nb_cols)
{
	// Test image size differ
	if ((nb_rows != nrows) || (nb_cols != ncols)) {
		if (bitmap != NULL) {
			delete [] bitmap;
			bitmap = NULL;
		}
	}

	if (nb_rows != nrows) {
		if (row != NULL)  {
			delete [] row;
			row = NULL;
		}
	}

	SetCols(nb_cols) ;
	SetRows(nb_rows) ;

	npixels=ncols*nrows;

	if (bitmap == NULL) bitmap = new Type[npixels] ;
	if (bitmap == NULL)
	{
		cerr << "Error in CMotion2DImage::Init(): cannot allocate memory" << endl;
		return false ;
	}

	if (row == NULL) row = new  Type*[nrows] ;
	if (row == NULL)
	{
		cerr << "Error in CMotion2DImage::Init(): cannot allocate memory" << endl;
		return false;
	}

	unsigned i ;
	for ( i =0  ; i < nrows ; i++)
		row[i] = bitmap + i*ncols ;

	return true;
}

/*!

  Allocates memory for an image of \e nb_rows rows and \e nb_cols columns and
  set all elements of the bitmap to \e value.

  \return true if successful, or false if memory cannot be allocated.

  \sa Init(unsigned, unsigned)

 */
template<class Type>
bool
CMotion2DImage<Type>::Init(unsigned nb_rows, unsigned nb_cols, Type value)
{

	bool res = Init(nb_rows, nb_cols) ;
	if (res != true) return res ;

	for (unsigned i=0  ; i < npixels ;  i++)
		bitmap[i] = value ;

	return true ;
}

/*!

  Allocates memory for an image of \e nb_rows rows and \e nb_cols
  columns. Elements of the bitmap are not initialized.

  If the image was already initialized, memory allocation is done
  only if the new image size is different, else we re-use the same
  memory space.

  \return true if successful, or false if memory cannot be allocated.

  \sa Init(unsigned, unsigned)

 */
template<class Type>
bool
CMotion2DImage<Type>::Resize(unsigned nb_rows, unsigned nb_cols)
{
	return Init(nb_rows, nb_cols);
}

/*!

  Computes the logical AND between the A et B images.

  C = A & B

  \return true if successful, or false if image size differs or if memory
  cannot be allocated.

 */
template<class Type>
bool
CMotion2DImage<Type>::And(CMotion2DImage<Type> &A, CMotion2DImage<Type> &B)
{
	if (A.GetRows() != B.GetRows())
		return false;
	if (A.GetCols() != B.GetCols())
		return false;

	bool res =  Init(A.GetRows(), A.GetCols());
	if (res != true)
		return res;

	for (unsigned i = 0; i < npixels; i ++) {
		bitmap[i] = A.bitmap[i] & B.bitmap[i];
	}

	return true;
}

/*!

  Computes a "specific" AND between the A et B images.

  \f[ C[i][j] = \left\{ \begin{array}{l} A[i][j] \mbox{\hspace{0.4cm}if\hspace{0.2cm}} B[i][j] = label \\ 0 \mbox{\hspace{1.2cm}} otherwise \end{array} \right. \f]


  \return true if successful, or false if image size differs or if memory
  cannot be allocated.

 */
template<class Type>
bool
CMotion2DImage<Type>::And(CMotion2DImage<Type> &A,
		CMotion2DImage<Type> &B,
		Type label)
		{
	if (A.GetRows() != B.GetRows())
		return false;
	if (A.GetCols() != B.GetCols())
		return false;

	bool res =  Init(A.GetRows(), A.GetCols());
	if (res != true)
		return res;

	for (unsigned i = 0; i < npixels; i ++) {
		if (B.bitmap[i] == label)
			bitmap[i] = A.bitmap[i];
		else
			bitmap[i] = 0;
	}

	return true;
		}

/*!

  Apply a median filter to the image. The filter size is given by the \e
  filterRowSize and \e filterColSize parameters.

  \return false if the filter size is bad (ie. filterRowSize or filterColSize
  are equal to zero), true if the filtering was successfully achieved.

 */
template<class Type>
bool CMotion2DImage<Type>::MedianFilter(unsigned filterRowSize,
		unsigned filterColSize)
		{
	// Subsampled image size
	unsigned _nrows = GetRows();
	unsigned _ncols = GetCols();
	int ik, jl;
	Type *tab;

	if (filterRowSize < 1)
		return false;
	if (filterColSize < 1)
		return false;

	if ((filterRowSize * filterColSize) == 1)
		return true;

	int halfFilterRowSize = filterRowSize / 2;
	int halfFilterColSize = filterColSize / 2;

	CMotion2DImage<Type> I;
	I.Resize(_nrows, _ncols);

	tab = new Type[filterRowSize*filterColSize];
	for (unsigned i=0; i < _nrows; i++) {
		for (unsigned j=0; j < _ncols; j++) {

			unsigned co=0;
			for (int k = -halfFilterRowSize;
					k < (int)(filterRowSize-halfFilterRowSize); k++) {
				for (int l = -halfFilterColSize;
						l < (int)(filterColSize-halfFilterColSize); l++) {
					ik=i+k;
					jl=j+l;
					if (ik >= 0 && ik < (int) _nrows && jl >= 0 && jl < (int) _ncols)
						tab[co++] = (*this)[ik][jl];
				}
			}
			sort(co, tab-1);
			I[i][j] = tab[(co >> 1) + 1];
		}
	}

	(*this) = I;
	delete [] tab;
	return true;
		}

/*!

  Assigns a copy of \e image to this image and returns a reference to this
  image.

 */
template<class Type>
void CMotion2DImage<Type>::operator=(const CMotion2DImage<Type> &image)
{
	Resize(image.GetRows(),image.GetCols()) ;

	unsigned i ;

	memcpy(bitmap, image.bitmap, image.npixels*sizeof(Type)) ;

	for (i=0; i<nrows; i++)
		row[i] = bitmap + i*ncols ;
}

/*!

  Set each element of the bitmap to \e x.
 */
template<class Type>
void CMotion2DImage<Type>::operator=(Type x)
{
	for (unsigned i=0 ; i < npixels ; i++)
		bitmap[i] = x ;
}
/*!

  Makes a difference between 2 images returns a reference to the difference
  image.

 */
template<class Type>
CMotion2DImage<Type> CMotion2DImage<Type>::operator-(const CMotion2DImage<Type> &image)
{

	CMotion2DImage<Type> D(GetRows(),GetCols());

	for (unsigned i=0; i<npixels; i++) {
		D.bitmap[i] = bitmap[i] - image.bitmap[i];
	}
	return D;
}
/*!

  Subsample the image. The subsampled image is given in I. The number of rows
  and columns of this image is divided by two. To determine the pixel value, we
  consider the mean value of the four neighbours.

 */
template<class Type>
void CMotion2DImage<Type>::Subsample()
{
	// Subsampled image size
	unsigned _nrows = GetRows() >> 1;
	unsigned _ncols = GetCols() >> 1;

	CMotion2DImage<Type> I;
	I.Resize(_nrows, _ncols);

	for (unsigned i=0; i < _nrows; i++) {
		for (unsigned j=0; j < _ncols; j++) {
			int u=0;
			for (unsigned k= 0; k <= 1;k++)
				for (unsigned l=0; l<= 1;l++) {
					u += (*this)[k+(i<<1)][l+(j<<1)];
				}
			u = (u / 4);
			I[i][j] = u;
		}
	}

	(*this) = I;
}

/*!
 * Print an error message saying that the image format is not supported
 */
template<class Type>
void CMotion2DImage<Type>::printUnsupportedImageError () {
	string png = "";
	string tif = "";
#ifndef __NO_IMAGEIO_PNG_
	png = " PNG,";
#endif
#ifndef __NO_IMAGEIO_TIF_
	tif = " TIFF,";
#endif
	cerr << "Error: Only" << tif << " " << png << " PNM (PGM P5 and PPM P6), RAW 8 bits and "
			<< endl << "RAW 16 bits image format are implemented..." << endl;
}


// Include for image ios
#include "Motion2DImage_PNM.h"
#include "Motion2DImage_RAW.h"
#include "Motion2DImage_PNG.h"
#include "Motion2DImage_TIFF.h"

#undef DEBUG_LEVEL1

// For template instantiation with Visual Studio
#if defined(MOTION2D_BUILD_SHARED_LIBS) && defined(MOTION2D_USE_MSVC)
template class MOTION2D_EXPORT CMotion2DImage<unsigned char>; 
template class MOTION2D_EXPORT CMotion2DImage<short>; 
#endif


#endif
