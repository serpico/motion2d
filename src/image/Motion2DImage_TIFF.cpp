/*

	  Copyright (c) 1995-2005 by INRIA.
	  All Rights Reserved.

	  This software was developed at:
	  IRISA/INRIA Rennes
	  Campus Universitaire de Beaulieu
	  35042 Rennes Cedex

	  http://www.irisa.fr

 */

#ifndef __NO_IMAGEIO_TIFF_

#include <stdio.h>
#include <stdlib.h>
#include <string>

#include <CMotion2DImage.h>
#include <Motion2DImage_TIFF.h>

#define cimg_display 0
#define cimg_use_tiff
//#define cimg_use_jpeg 0
//#define cimg_use_png 0
#include "CImg.h"
using namespace cimg_library;


#define round(x)  ((x-floor(x) > 0.5 ) ? ceil(x) : floor(x))

#define DEBUG_LEVEL1 0

/*!
	  \file Motion2DImage_TIFF.cpp
	  \brief Definition of TIFF image format input/output functions.
 */


/*
	  Read the contents of the TIFF filename, allocate memory for the corresponding
	  gray level image, convert the data in gray level, and set the bitmap whith
	  the gray level data. That means that the image \e I is a "black and white"
	  rendering of the original image in \e filename, as in a black and white
	  photograph. In case of TIFF color images, the gray level conversion is
	  obtained by applying the quantization formula: \f$0,299 r + 0,587 g + 0,114
	  b\f$.

	  \return false if an error occurs, or true otherwise.

	  If the image has been already initialized, memory allocation is done
	  only if the new image size is different, else we re-use the same
	  memory space.

	  \sa ReadPGM(), ReadPPM(), WritePNG()

 */

int ReadTIFF(CMotion2DImage<unsigned char> &I, const char *filename, int frame) {
	TIFFSetWarningHandler(0);
	CImg<unsigned char> tifImg (filename);
	if (!tifImg) {
		fprintf(stderr,"ReadTIFF: error, can't open file");
		return false;
	}

	unsigned int height = tifImg._height;
	unsigned int width = tifImg._width;

	if (!I.Resize(height, width)) {
		fprintf(stderr,"ReadTIFF: error, can't allocate memory");
		return false;
	}

	if (tifImg._spectrum == 1) {
		for (unsigned int w=0; w<width;w++) {
			for (unsigned int h=0; h<height;h++) {
				I.bitmap[h*width+w] = tifImg(w,h,frame);
			}
		}
	/*} else if (tifImg._spectrum == 3) { // assume it is RGB >>NOT TESTED<<
		for (unsigned int w=0; w<width;w++) {
			for (unsigned int h=0; h<height;h++) {
				I.bitmap[h*width+w] = (unsigned char)((float)(tifImg(w,h,frame,0))*0.299 + (float)(tifImg(w,h,frame,1))*0.587 + (float)(tifImg(w,h,frame,2))*0.114);
			}
		}*/
	} else {
		fprintf(stderr,"ReadTIFF: only grayscale images");
		return false;
	}
	return true;
}

/*

	  Read the contents of the TIFF filename, allocate memory for the corresponding
	  gray level image, convert the data in gray level, and set the bitmap whith
	  the gray level data. That means that the image \e I is a "black and white"
	  rendering of the original image in \e filename, as in a black and white
	  photograph. In case of TIFF color images, the gray level conversion is
	  obtained by applying the quantization formula: \f$0,299 r + 0,587 g + 0,114
	  b\f$.

	  \return false if an error occurs, or true otherwise.

	  If the image has been already initialized, memory allocation is done
	  only if the new image size is different, else we re-use the same
	  memory space.

	  \sa ReadPGM(), ReadPPM(), WritePNG()

 */
int ReadTIFF(CMotion2DImage<short> &I, const char *filename, int frame) {
	TIFFSetWarningHandler(0);
	CImg<short> tifImg (filename);
	if (!tifImg) {
		fprintf(stderr,"ReadTIFF: error, can't open file");
	}

	unsigned int height = tifImg._height;
	unsigned int width = tifImg._width;

	if (!I.Resize(height, width)) {
		fprintf(stderr,"ReadTIFF: error, can't allocate memory");
	}

	for (unsigned int w=0; w<width;w++) {
		for (unsigned int h=0; h<height;h++) {
			I.bitmap[h*width+w] = tifImg(w,h,frame);
		}
	}

	return true;
}


/*

	  Write the content of the bitmap in the file which name is given by \e
	  filename. This function writes a TIFF file.

	  \warning The resulted PNG image is a gray level image.

	  \return false if an error occurs, or true otherwise.

	  \sa ReadPNG(), WritePGM(), WritePPM()

 */
int WriteTIFF(CMotion2DImage<unsigned char> &I, const char *filename) {
	TIFFSetWarningHandler(0);
	unsigned int height = I.GetRows();
	unsigned int width = I.GetCols();

	CImg<unsigned char> tifImg (width, height);
	if (!tifImg) {
		fprintf(stderr,"WriteTIFF: error, can't allocate memory");
	}

	for (unsigned int w=0; w<width;w++) {
		for (unsigned int h=0; h<height;h++) {
			tifImg(w,h) = I.bitmap[h*width+w];
		}
	}

	tifImg.save(filename);
	return true;
}

/*
	Write the content of the bitmap in the file which name is given by \e
	filename. This function writes a TIFF file.

	\warning The resulted TIFF image is a gray level image.

	\return false if an error occurs, or true otherwise.

			\sa ReadPNG(), WritePGM(), WritePPM()

 */
int WriteTIFF(CMotion2DImage<short> &I, const char *filename) {
	TIFFSetWarningHandler(0);
	unsigned int height = I.GetRows();
	unsigned int width = I.GetCols();

	CImg<short> tifImg (width, height);
	if (!tifImg) {
		fprintf(stderr,"WriteTIFF: error, can't allocate memory");
	}

	for (unsigned int w=0; w<width;w++) {
		for (unsigned int h=0; h<height;h++) {
			tifImg(w,h) = I.bitmap[h*width+w];
		}
	}

	tifImg.save(filename);
	return true;
}
#endif
