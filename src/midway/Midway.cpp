#include "Midway.h"

void print_histo (double* h, int nb_bins, double val_max) {
	double delta = val_max / nb_bins;
	for (int i=0; i < nb_bins; i++) {
		cout << "bin " << i << " from " << i*delta << " to " << i*delta+delta << " count " << h[i] << endl;
	}
}


bool Midway::histogram_cumul(CMotion2DImage<short>& I, double* histo_cumul, int nb_bins, double val_max, bool normalized)
{
	double* histo;
	histo = (double*) malloc (nb_bins*sizeof(double)); // TODO check memory alloc
	if (!histo) {
		return false;
	}

	Midway::histogram(I, histo, nb_bins, val_max);

	int N;
	if(normalized == 0)
		N = 1;
	else
		N = I.GetCols() * I.GetRows();

	histo_cumul[0] = 0;
	for(int i=1; i<nb_bins; i++) {
		histo_cumul[i] = (histo_cumul[i-1] + histo[i]);
	}
	for(int i=1; i<nb_bins; i++) {
		histo_cumul[i] /= N;
	}

	free (histo);
	return true;
}

void Midway::histogram(CMotion2DImage<short>& I, double* histo, int nb_bins, double val_max) {
	for (unsigned int i = 0; i < nb_bins; i++) {
		histo[i] = 0;
	}

	unsigned int height = I.GetRows();
	unsigned int width = I.GetCols();
	for (unsigned int w=0; w<width;w++) {
		for (unsigned int h=0; h<height;h++) {
			short val = I[h][w];
			++histo[val==val_max?nb_bins-1:(int)(val*nb_bins/val_max)];
		}
	}
}


// I1 and I2 have same dimensions
void Midway::midway_equalization(CMotion2DImage<short>& I1, CMotion2DImage<short>& I2)
{
	double *H1, *H2;

	//// Imax = max(I1.max(), I2.max()); ////
	unsigned int Imax = 0;
	unsigned int height = I1.GetRows();
	unsigned int width = I1.GetCols();
	for (unsigned int w=0; w<width;w++) {
		for (unsigned int h=0; h<height;h++) {
			if (I1[h][w] > Imax) Imax = I1[h][w];
			if (I2[h][w] > Imax) Imax = I2[h][w];
		}
	}

	int nb_bins = Imax+10;
	H1 = (double*) malloc (nb_bins*sizeof(double)); // TODO check memory not alloc
	H2 = (double*) malloc (nb_bins*sizeof(double));

	if (!H1 || !H2) {
		cout << "Error, cannot allocate memory for midway histograms / " << "midway equalization not done" << endl;
		return;
	}

	if (!Midway::histogram_cumul(I1, H1, nb_bins, Imax+9, 1)) {
		cout << "Error, cannot allocate memory for midway histograms / " << "midway equalization not done" << endl;
		return;
	}
	if (!Midway::histogram_cumul(I2, H2, nb_bins, Imax+9, 1)) {
		cout << "Error, cannot allocate memory for midway histograms / " << "midway equalization not done" << endl;
		return;
	}

//	cout << "H1" << endl;
//	print_histo(H1, nb_bins, Imax+9);
//	cout << "H2" << endl;
//	print_histo(H2, nb_bins, Imax+9);
//	cout << endl << endl;

	double H1_xy, H2_xy, H1_inv, H2_inv;

	for (int x = 0; x < I1.GetCols(); x++) {
		for (int y = 0; y < I1.GetRows(); y++) {
			// Transformation of I1
			H1_xy = H1[I1[y][x]];
			int i = 0;
			while(H2[i] < H1_xy)
			{
				H2_inv = i;
				i++;
			}
			I1[y][x] = (I1[y][x] + H2_inv)/2;

			// Transformation of I2
			H2_xy = H2[I2[y][x]];
			i = 0;
			while(H1[i] < H2_xy)
			{
				H1_inv = i;
				i++;
			}
			I2[y][x] = (I2[y][x] + H1_inv)/2;
		}
	}
	free (H1);
	free (H2);
}
