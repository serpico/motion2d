#############################################################################
#
# $Id: FindBIT3.cmake,v 1.3 2006/11/07 14:13:09 fspindle Exp $
#
# Copyright (C) 1998-2006 Inria. All rights reserved.
#
# This software was developed at:
# IRISA/INRIA Rennes
# Projet Lagadic
# Campus Universitaire de Beaulieu
# 35042 Rennes Cedex
# http://www.irisa.fr/lagadic
#
# This file is part of the ViSP toolkit
#
# This file may be distributed under the terms of the Q Public License
# as defined by Trolltech AS of Norway and appearing in the file
# LICENSE included in the packaging of this file.
#
# Licensees holding valid ViSP Professional Edition licenses may
# use this file in accordance with the ViSP Commercial License
# Agreement provided with the Software.
#
# This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
# WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
#
# Contact visp@irisa.fr if any conditions of this licensing are
# not clear to you.
#
# Description:
# Try to find library for key coding/decoding.
# Once run this will define: 
#
# KEYCODEC_FOUND
# KEYCODEC_INCLUDE_DIR
# KEYCODEC_LIBRARIES
#
# Authors:
# Fabien Spindler
#
#############################################################################

IF(WIN32)
FIND_PATH(KEYCODEC_INCLUDE_DIR Key.h
  $ENV{KEYCODEC_HOME}/include
  z:/soft/Equipe/Crypt/KeyCodec-build/include
  )
#MESSAGE("DBG KEYCODEC_INCLUDE_DIR=${KEYCODEC_INCLUDE_DIR}")  
  
FIND_LIBRARY(KEYCODEC_LIBRARY
  NAMES KeyCodec
  PATHS 
  $ENV{KEYCODEC_HOME}/lib
  z:/soft/Equipe/Crypt/KeyCodec-build/lib
  )

#MESSAGE("DBG KEYCODEC_LIBRARY=${KEYCODEC_LIBRARY}")

## --------------------------------

IF(KEYCODEC_LIBRARY)
  SET(KEYCODEC_LIBRARIES ${KEYCODEC_LIBRARY})
ELSE(KEYCODEC_LIBRARY)
#  MESSAGE(SEND_ERROR "KeyCodec library not found.")
ENDIF(KEYCODEC_LIBRARY)

IF(NOT KEYCODEC_INCLUDE_DIR)
#  MESSAGE(SEND_ERROR "KeyCodec include dir not found.")
ENDIF(NOT KEYCODEC_INCLUDE_DIR)

IF(KEYCODEC_LIBRARIES AND KEYCODEC_INCLUDE_DIR)
  SET(KEYCODEC_FOUND TRUE)
ELSE(KEYCODEC_LIBRARIES AND KEYCODEC_INCLUDE_DIR)
  SET(KEYCODEC_FOUND FALSE)
ENDIF(KEYCODEC_LIBRARIES AND KEYCODEC_INCLUDE_DIR)

MARK_AS_ADVANCED(
  KEYCODEC_INCLUDE_DIR
  KEYCODEC_LIBRARIES
  KEYCODEC_LIBRARY
  )
ENDIF(WIN32)
